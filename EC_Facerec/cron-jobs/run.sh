#!/bin/bash
#logs are saved in /home/rajneesh/DLProjects/EC_Facerec/cron-jobs/cronlog_outlook.txt
for pid in $(ps -ef | grep "converge.py" | awk '{print $2}'); do kill -9 $pid; done
for pid in $(ps -ef | grep "redis_meets_update_parallel.py" | awk '{print $2}'); do kill -9 $pid; done
date
source /home/rajneesh/anaconda3/bin/activate /home/rajneesh/anaconda3/envs/py27 && python /home/rajneesh/DLProjects/EC_Facerec/cron-jobs/redis_meets_update_parallel.py
