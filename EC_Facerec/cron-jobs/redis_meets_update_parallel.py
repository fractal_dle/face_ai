import redis
import requests
import json
import time,pytz
from datetime import datetime,timedelta
from dateutil.tz import *
import unicodedata
from multiprocessing import Pool 

redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)
all_keys=[k for k in redis_db.scan_iter("fms_*")]
tz = pytz.timezone("Asia/Kolkata")
now = datetime.now(tz)+timedelta(minutes=20)
print '20mins plus'
print now
asia_hr=now.strftime('%H')
now=now.astimezone(pytz.timezone('UTC'))
day=now.strftime('%Y-%m-%d')
hour=now.strftime('%H')
minute='30'
url = "http://172.16.2.166:8197/get_usercalendar1/"

def redis_load(id,meet,tag):
	if meet:
		print '4'
		if tag==0:
			redis_db.set("evnt_"+id,meet[0])
		else:
			redis_db.set("evnt_"+id,meet[0],ex=2100)

def events_util(id,user_mail):	
	payload = "email="+user_mail+"%40fractalanalytics.com&select=Subject%2COrganizer%2CLocation%2CStart%2CEnd&start_date="+day+"T"+hour+"%3A"+minute+"&end_date="+day+"T23%3A30"
	headers = { 'content-type': "application/x-www-form-urlencoded",'cache-control' : 'no-cache'}
	print '1'
	response = requests.request("POST", url, data=payload, headers=headers)
	meets=[]
	ex_tag=0
	print '2'
	if response.status_code==200:
		try:
			events=response.json()['value']
			for i in events:
				print '3'
				temp=i['start']['dateTime']
				meet_time=datetime.strptime(temp[0:temp.rfind(':')], "%Y-%m-%dT%H:%M").time().strftime("%H:%M")
				if int(meet_time.split(':')[0])<int(asia_hr):
					continue
				if meet_time==asia_hr+":00":
					ex_tag=1 
				if i['subject']:
					val = unicodedata.normalize('NFKD',i['subject']+" at "+meet_time+"hrs").encode('ascii','ignore')
				else: 
					val ='MEET at '+meet_time+"hrs"
				if i['location']['displayName']:
					val=val+" ("+unicodedata.normalize('NFKD',i['location']['displayName']).encode('ascii','ignore')+")"
				meets.append(val)
				break
		except:
			print (id,"err")
	else:
		print(id,response.status_code)
	redis_load(id,meets,ex_tag)

def events_util2(id,user_mail):	
	payload = "email="+user_mail+"%40fractalanalytics.com&select=Subject%2COrganizer%2CLocation%2CStart%2CEnd&start_date="+day+"T"+hour+"%3A"+minute+"&end_date="+day+"T23%3A30"
	headers = { 'content-type': "application/x-www-form-urlencoded",'cache-control' : 'no-cache'}
	print '1'
	response = requests.request("POST", url, data=payload, headers=headers)
	meets=[]
	ex_tag=0
	print '2'
	if response.status_code==200:
		try:
			events=response.json()['value']
			for i in events:
				print '3'
				temp=i['start']['dateTime']
				meet_time=datetime.strptime(temp[0:temp.rfind(':')], "%Y-%m-%dT%H:%M").time().strftime("%H:%M")
				if int(meet_time.split(':')[0])<int(asia_hr):
					continue
				if meet_time==asia_hr+":00":
					ex_tag=1 
				if i['subject']:
					val = unicodedata.normalize('NFKD',i['subject']+" at "+meet_time+"hrs").encode('ascii','ignore')
				else: 
					val ='MEET at '+meet_time+"hrs"
				if i['location']['displayName']:
					val=val+" ("+unicodedata.normalize('NFKD',i['location']['displayName']).encode('ascii','ignore')+")"
				meets.append(val)
				break
		except:
			print (id,"err")
	else:
		print(id,response.status_code)
	redis_load(id,meets,ex_tag)

def events(id):
	user_mail=str(redis_db.hget(id,'email')).split("@",1)[0]
	id=id[4:]
	print (id,user_mail,day,hour,minute,asia_hr)
	events_util(id,user_mail)

def clear_evnts():
	#keys = redis_db.keys('evnt_*')  # this returns a list
	evnt_keys =[o for o in redis_db.scan_iter("evnt_*")]
	print evnt_keys
	print len(evnt_keys)
	if evnt_keys:
		print 'here'
		for i in evnt_keys:
			redis_db.delete(i)

def main():
	#print all_keys
	print 'redis_meeet_update'
	pool = Pool(processes=10)
	pool.map(events, all_keys)
	pool.close() 
	pool.join()

def temp_main():
	#print all_keys
	for id in all_keys:
		events(id)

if __name__ == '__main__':
	start_time=time.time()
	clear_evnts()
	main()
	print("---%s seconds---" % (time.time()-start_time))
	print datetime.now(tz)