import requests
import json
import redis
import unicodedata
import pandas as pd
import csv
import redis_meets_update_parallel

converge_api_endpoint = 'https://converge.namely.com/api/v1/reports{0}'
redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)

def clear_keys_pat(pattern):
	keys =[o for o in redis_db.scan_iter(pattern)]
	print len(keys)
	if keys:
		for i in keys:
			redis_db.delete(i)

def redis_load(employee_data):
	not_insert=[]
	c=0
	for i in employee_data:
		c=c+1
		try:
			user={'status':str(i[0]),'name':str(i[2]),'email':str(i[3]),'doj':str(i[4]),'location':str(i[5])}
			redis_db.hmset("fms_"+str(i[1]),user)
		except:
			not_insert.append(str(i))
	print c
	extra=("fms_Admin Helpdesk","fms_IT Helpdesk","fms_None","fms_USVISA","fms_TRVISA","fms_TRDESK","fms_HRMIS","fms_FRABOA","fms_FRACTAL HC","fms_MARKETING","fms_FAA","fms_BREAKFREE","fms_","fms_MISUSER","fms_FREXEC")
	redis_db.delete(*extra)
	print not_insert

def csv_load(employee_data):
    with open("active_emp.csv", "wb") as f:
        writer = csv.writer(f)
        writer.writerows(employee_data)

def make_api_call(url):
    headers = { 'ID' : 'de54bc2c-daf1-4d26-908d-734899b79597',
              'Authorization' : 'Bearer o9rAPeLCcLnhc8HLVShLJNB9mIL1mvgx3vnHnnftuHDqIL0jZDRo1K5DdSRUnXTs',
              'Accept' : 'application/json'}
  
    r = requests.get(url, headers = headers)
    reports=r.json()['reports']
    if (r.status_code == 200):
    	for i in reports:
    		redis_load(i['content'])
    		#csv_load(i['content'])
    		return len(i['content'])
    else:
    	return "{0}: {1}".format(r.status_code, r.text)

def main():
	active_url = converge_api_endpoint.format('/de54bc2c-daf1-4d26-908d-734899b79597')
	active_mumbai=converge_api_endpoint.format('/ef626569-e636-4030-abd6-6421d17c8ba8')
	inactive_url= converge_api_endpoint.format('/0519c108-b065-4b17-a203-f20274fa9088')
	clear_keys_pat("fms_*")
	make_api_call(active_mumbai)
	redis_meets_update_parallel.main()

if __name__ == "__main__":
	main()


#handle such examples [u'Active Employee', None, u'Hrishikesh  Narkar', u'hrishikesh.narkar@mgconsulting.in\xa0', u'05/06/2018', u'Mumbai']