#!/bin/bash
process=$1
MAIN_DIR='/data/fractal_git/face-recognition/EC_Facerec'
for pid in $(ps -ef | grep "face_display.py" | awk '{print $2}'); do kill -9 $pid; done
for pid in $(ps -ef | grep "fractal_face_rec_with_events_multiprocess.py" | awk '{print $2}'); do kill -9 $pid; done
trap 'kill %1; kill %2' SIGINT
source activate py27 && python $MAIN_DIR/face_display.py & source activate py27 && python $MAIN_DIR/fractal_face_rec_with_events_multiprocess.py $process
