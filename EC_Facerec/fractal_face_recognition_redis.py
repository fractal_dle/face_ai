#!/usr/bin/python

import sys
import os
import dlib
import glob
#from skimage import io
import pickle
import cv2
import numpy as np
import colorsys
import time
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import random
import cPickle

import redis

def face_prediction(clf, face_encodings, face_locations, PROB_THRESH = .5):
	
	probas = clf.predict_proba(np.array(face_encodings))
	max_probas = np.max(probas, axis=1) 
	
	is_recognized = [max_probas[i] > PROB_THRESH for i in range(max_probas.shape[0])]

	# predict classes and cull classifications that are not with high confidence
	return [(pred, loc, prob) if rec else ("Other", loc, prob) for pred, loc, prob, rec in zip(clf.predict(face_encodings), face_locations, max_probas, is_recognized)]


def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def _main():
	
	#create a connection to the localhost Redis server instance, by default it runs on port 6379
	redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)

	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	cnn_face_detector_path = './models/mmod_human_face_detector.dat'
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detector_path)

	# Initialize dlib shape predictor
	predictor_path = './models/shape_predictor_68_face_landmarks.dat'
	shape_predictor = dlib.shape_predictor(predictor_path)

	# Initialize cnn face detector
	face_rec_model_path = './models/dlib_face_recognition_resnet_model_v1.dat'
	facerec = dlib.face_recognition_model_v1(face_rec_model_path)

	# Load face SVM based face predictor
	#model_save_path='../models/svc_test3.pkl'
	model_save_path='./models/model_82_7th.pkl'
	with open(model_save_path, 'rb') as f:
		clf = pickle.load(f)

	image_size = (720, 480)
	
	DETECTION_MODEL = 'cnn' # or 'cnn'
	UPSAMPLE_FACTOR = 1
	PROB_THRESH = 0.65

	#video_capture = cv2.VideoCapture('http://172.16.6.116:8080/video?dummy=param.mjpg')
	video_capture = cv2.VideoCapture(0)
	process_this_frame = True

	while(True):

		ret, frame = video_capture.read()
		#img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		height, width, _ = frame.shape

		font = ImageFont.truetype(font='/home/rajneesh/DLProjects/Utilities/font/FiraMono-Medium.otf', size=np.floor(5e-2 * height + 0.5).astype('int32'))
		thickness = (width + height) // 300

		
		if process_this_frame:
		
			if DETECTION_MODEL == 'cnn':
				detector = cnn_face_detector			
			dets = face_detection(detector, frame, UPSAMPLE_FACTOR)

			# Now process each face we found.
			face_encodings = []
			face_locations = []
			for k, d in enumerate(dets):
				
				if DETECTION_MODEL == 'cnn':
					left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
				else:
					left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
				
				print "Width: {} Height: {}".format(right-left, bottom-top)
				# Ignore too small faces
				if right - left < 60 and bottom - top < 60:
					continue				

				# Get the landmarks/parts for the face in box dd.	
				dd = dlib.rectangle(left, top, right, bottom)
				shape = shape_predictor(frame, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = facerec.compute_face_descriptor(frame, shape, 10)

				face_encodings.append(face_descriptor)
				face_locations.append((left, top, right, bottom))

			pil_image = Image.fromarray(frame)
			draw = ImageDraw.Draw(pil_image)
			
			if len(face_encodings) > 0:
				preds = face_prediction(clf, face_encodings, face_locations, PROB_THRESH)
				

				# Plot face recognition
				for pred in preds:
					if pred[0] == 'Other':
						continue

					predictions = pred[0].split('_')
					if redis_db.get(predictions[0]):
						#print redis_db.get(predictions[0])
						pass
					else:
						redis_db.set(predictions[0], predictions[1]+'_'+predictions[2], ex=5)
					
					predicted_face = pred[0]
					face_locations = pred[1]
					pred_probablity = pred[2]
					
					left, top, right, bottom = face_locations[0], face_locations[1], face_locations[2], face_locations[3]

					label = '{} {:.2f}'.format(predicted_face, pred_probablity)
					#label = predicted_face
					label_size = draw.textsize(label, font)
					label_w, label_h = label_size
					#print np.floor(4e-2 * height + 0.5), thickness

					for i in range(thickness):
						draw.rectangle([left + i, top + i, right - i, bottom - i], outline=(255, 255, 255))
										
					new_bottom = min(bottom+label_h+2, height-2)
					label_origin_bottom = new_bottom-label_h-2

					if label_w < (right - left):
						label_w = (right - left)

					draw.rectangle([left, label_origin_bottom, left+label_w, new_bottom], fill=(0, 0, 150))					
					draw.text(np.array([left, bottom - 2]), label, fill=(255, 255, 255), font=font)

		process_this_frame = not process_this_frame
		img_display = cv2.resize(np.array(pil_image), image_size)
		cv2.imshow('FaceRecognition', np.array(img_display))
		#videoWriter.write(img_display)       				
		
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

if __name__ == '__main__':
	_main()

