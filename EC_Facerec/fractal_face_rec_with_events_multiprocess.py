#!/usr/bin/python

import sys
import os
import dlib
import glob
#from skimage import io
import pickle
import cv2
import numpy as np
import colorsys
import time
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import random
import cPickle

import redis
import multiprocessing
from functools import partial
from contextlib import contextmanager

#Directory Variables
main_dir='/data/fractal_git/face-recognition/EC_Facerec'
model_dir='/data/fractal_git/face-recognition/models'

#create a connection to the localhost Redis server instance, by default it runs on port 6379
redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)
DETECTION_MODEL = 'dlib' # or 'cnn'
UPSAMPLE_FACTOR = 1
PROB_THRESH = 0.65
no_processes = int(sys.argv[1])

# Load face SVM based face predictor
#model_save_path='./models/model_230_500.pkl'
model_save_path=model_dir+'/latest/2018-06-15_1_model_248_500.pkl'
with open(model_save_path, 'rb') as f:
	clf = pickle.load(f)


def face_prediction(faces, PROB_THRESH=.5):
	face_encodings=faces[0]
	face_location=faces[1]
	face_encodings = np.array(face_encodings).reshape(1,-1) 
	probas = clf.predict_proba(np.array(face_encodings))
	max_probas = np.max(probas, axis=1) 
	is_recognized = [max_probas > PROB_THRESH]

	max_probas = "{0:.2f}".format(round(max_probas[0].tolist(), 2))
	prediction = str(clf.predict(face_encodings)[0])

	if is_recognized[0][0]:
		out_pred = prediction,face_location, max_probas
	else:
		out_pred = 'Other',face_location, max_probas

	return out_pred


def face_detection(detector1, image, upsample_factor):
	return detector1(image, upsample_factor)

def compute_128D(d,frame,shape_predictor,facerec):
	if DETECTION_MODEL == 'cnn':
		left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
	else:
		left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
	print "Width: {} Height: {}".format(right-left, bottom-top)
	# Ignore too small faces
	if right - left < 60 and bottom - top < 60:
		return []
	# Get the landmarks/parts for the face in box dd.
	dd = dlib.rectangle(left, top, right, bottom)
	shape = shape_predictor(frame, dd)
	# Compute face encoding (128D vectors)
	face_descriptor = facerec.compute_face_descriptor(frame, shape, 10)
	face_location=(left, top, right, bottom)
	return [face_descriptor,face_location]
	

def redis_update(pred):
	# Plot face recognition
	if pred[0] == 'Other':
		return
	else:
		predictions = pred[0].split('_')
		if redis_db.get("dis"+predictions[0]):
			return
		else:
			if redis_db.get("evnt_"+predictions[0]):
				redis_db.set("dis"+predictions[0], predictions[1]+'_'+predictions[2]+' '+str(redis_db.get("evnt_"+predictions[0])), ex=5)
			else:
				redis_db.set("dis"+predictions[0], predictions[1]+'_'+predictions[2], ex=5)
					
def label_plot(pred,draw,font,thickness,height):
	predicted_face=pred[0]
	face_locations=pred[1]
	pred_probability=pred[2]
	left, top, right, bottom = face_locations[0], face_locations[1], face_locations[2], face_locations[3]
	label = '{} {:.2f}'.format(predicted_face, float(pred_probability))
	#label = predicted_face
	label_size = draw.textsize(label, font)
	label_w, label_h = label_size
	#print np.floor(4e-2 * height + 0.5), thickness

	for i in range(thickness):
		draw.rectangle([left + i, top + i, right - i, bottom - i], outline=(255, 255, 255))
										
	new_bottom = min(bottom+label_h+2, height-2)
	label_origin_bottom = new_bottom-label_h-2

	if label_w < (right - left):
		label_w = (right - left)

	draw.rectangle([left, label_origin_bottom, left+label_w, new_bottom], fill=(0, 0, 150))					
	draw.text(np.array([left, bottom - 2]), label, fill=(255, 255, 255), font=font)

@contextmanager
def poolcontext(*args, **kwargs):
	pool = multiprocessing.Pool(*args, **kwargs)
	yield pool
	pool.terminate()

def _main():

	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	cnn_face_detector_path = model_dir+'/mmod_human_face_detector.dat'
	#cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detector_path)

	# Initialize dlib shape predictor
	predictor_path = model_dir+'/shape_predictor_68_face_landmarks.dat'
	shape_predictor = dlib.shape_predictor(predictor_path)

	# Initialize cnn face detector
	face_rec_model_path = model_dir+'/dlib_face_recognition_resnet_model_v1.dat'
	facerec = dlib.face_recognition_model_v1(face_rec_model_path)
	
	image_size = (720, 480)
	

	process_this_frame = True

	try:
		cam = cv2.VideoCapture(1)
	except IOError:
		cam = cv2.VideoCapture(0)
	
	while(True):
		ret, frame = cam.read()
		print ret
		height, width, _ = frame.shape
	
		#img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		#print ret,frame
		font = ImageFont.truetype(font=main_dir+'/font/FiraMono-Medium.otf', size=np.floor(5e-2 * height + 0.5).astype('int32'))	
		thickness = (width + height) // 300

		
		if process_this_frame:
		
			if DETECTION_MODEL == 'cnn':
				detector = cnn_face_detector			
			dets = face_detection(detector, frame, UPSAMPLE_FACTOR)
			
			# Now process each face we found.
			face_encodings = []
			face_locations = []
			for k, d in enumerate(dets):
				face=compute_128D(d,frame,shape_predictor,facerec)
				if face:
					face_encodings.append(face[0])
					face_locations.append(face[1])

			pil_image = Image.fromarray(frame)
			draw = ImageDraw.Draw(pil_image)

			faces=zip(face_encodings,face_locations)			
			if len(face_encodings)>0:
				with poolcontext(processes=no_processes) as pool:
					res=pool.map(partial(face_prediction, PROB_THRESH=PROB_THRESH), faces)
				
				pool = multiprocessing.Pool(processes=no_processes)
				pool.map(redis_update, res)
				pool.close() 
				pool.join()
			
				for pred in res:
					if pred[0] == 'Other':
						continue
					label_plot(pred,draw,font,thickness,height)

		process_this_frame = not process_this_frame
		img_display = cv2.resize(np.array(pil_image), image_size)
		cv2.imshow('FaceRecognition', np.array(img_display))
		#videoWriter.write(img_display)       				
		
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

if __name__ == '__main__':
	_main()

