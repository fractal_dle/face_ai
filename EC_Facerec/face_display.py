from flask import Flask,render_template,request, json
from PIL import Image
import os
import time
import cPickle
import redis

app = Flask(__name__)
#create a connection to the localhost Redis server instance, by default it runs on port 6379
redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)


@app.route('/')
def welcome():
	return 'Welcome to Fractal Analytics'

@app.route('/index')
def signUp():
	return render_template('index.html')

@app.route('/example')
def example():
	'''
	try:
		faces = cPickle.load(open('faces.p', 'rb'))
		print faces
		d = {'Id': list(faces)}
		os.remove('faces.p')
		return json.dumps(d)
	except:
		return json.dumps({'Id': list()})
	'''
	try:
		a=list(str(k).split('dis')[1]+' '+str(redis_db.get(k)) for k in redis_db.scan_iter("dis*"))
		print a
		return json.dumps({'Id': a})
		#return json.dumps({'Id': list(all_keys)})
	except:
		return json.dumps({'Id': list()})
		

if __name__ == '__main__':

	app.run(
			host="127.0.0.1",
			#host="172.16.7.134",
			port=int("4012"),
			debug=True
			)
