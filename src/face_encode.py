import dlib
import sys,os,cv2
import pandas as pd
from datetime import datetime
import subprocess

def face_encoder(main_dir, shape_predictor, facerec_model):
	dir_path = main_dir
	know_face_encoding = []
	known_faces = []
	column_list = list(range(128))
	column_list.append('label')
	df = pd.DataFrame(columns=column_list)
	i = 0

	all_dirs = os.listdir(dir_path)
	for each_dir in all_dirs:
		all_images = os.listdir(dir_path+each_dir)
		name = each_dir
		print "processing:", name
		for each_image in all_images:
			try:
				image_path  = dir_path+each_dir+'/'+each_image
				image = cv2.imread(image_path)		
				height, width, _ = image.shape
				dd = dlib.rectangle(0, 0, width, height)
				shape = shape_predictor(image, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = list(facerec_model.compute_face_descriptor(image, shape))
				face_descriptor.append(name)
				df.loc[i] = face_descriptor
				i += 1
			except:
				print "skipped"
	return df

def get_latest_encoding_filename(encoding_dir):
	min=1000
	day_tag=0
	version=-100
	cur_date=int(''.join(datetime.now().date().isoformat().split('-')))
	for i in os.listdir(encoding_dir):
		temp= cur_date- int(''.join(i.split('_')[0].split('-')))
		v=i.split('_')[1]
		if temp==0:
			day_tag=1
			if version < v:
				file=i
				version=v
				min=temp
		elif temp<min:
			min=temp
			file=i
			version=v
	return file,day_tag,version

def get_new_encoding_filename(file,add_count,day_tag,version):
	fname=file.split('_')
	fname[0]=datetime.now().date().isoformat()
	fname[4]=str(int(fname[4])+add_count)
	if day_tag==1:
		fname[1]=str(int(version)+1)
	else:
		fname[1]='1'
	return '_'.join(fname)

def main(main_dir,encodings_file,predictor_path,face_rec_model_path):

	# Initialize dlib shape predictor
	shape_predictor = dlib.shape_predictor(predictor_path)

	# Initialize cnn face detector
	facerec_model = dlib.face_recognition_model_v1(face_rec_model_path)
	return face_encoder(main_dir, shape_predictor, facerec_model)

if __name__=="__main__":
	crop_dir=sys.argv[1]
	predictor_path=sys.argv[2]
	face_rec_model_path=sys.argv[3]
	encoding_dir=sys.argv[4]
	file,day_tag,version=get_latest_encoding_filename(encoding_dir)
	df_new = main(crop_dir,file,predictor_path,face_rec_model_path)
	df_old=pd.read_csv(encoding_dir+file)
	df_new.to_csv(encoding_dir+'test.csv')
	df_new=pd.read_csv(encoding_dir+'test.csv')
	df_new=df_new.iloc[:,1:]
	df_old=df_old.iloc[:,1:]
	df_all=df_old.append(df_new,ignore_index=True)
	subprocess.Popen(['rm', '-f', encoding_dir+'test.csv'])
	df_all.to_csv(encoding_dir+get_new_encoding_filename(file,len(set(df_new['label'])),day_tag,version), sep=',', encoding='utf-8')
