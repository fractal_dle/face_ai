#!/bin/bash

#
# This script will help you to do model training for new users
#

NONE='\033[00m'
RED='\033[01;31m'
GREEN='\033[01;32m'
YELLOW='\033[01;33m'
PURPLE='\033[01;35m'
CYAN='\033[01;36m'
WHITE='\033[01;37m'
BOLD='\033[1m'
UNDERLINE='\033[4m'

function fsf_err
{
    /bin/echo -e ${RED} `date "+%Y-%m-%e %T: [ERR]"` ${NONE} $*
}

function fsf_info
{
    /bin/echo -e ${GREEN} `date "+%Y-%m-%e %T: [INFO]"` ${NONE} $*
}

function fsf_warn
{
    /bin/echo -e ${YELLOW} `date "+%Y-%m-%e %T: [WARN]"` ${NONE} $*
}

function clear_dir
{
    echo $(echo -n DE:AD:BE:EF ; for i in `seq 1 2` ;
    do echo -n `echo ":$RANDOM$RANDOM" | cut -n -c -3` ;done)
}

#Variable with server
CODE_DIR='/data/fractal_git/face-recognition/src'
MAIN_DIR='/data/fractal_git/face-recognition'
DATABASE_DIR='/data/FacerecImages'

FACE_POS_DIR=$MAIN_DIR/Face-Pose-Net
encoding_dir=$MAIN_DIR/models/encodings/
trained_model_dir=$MAIN_DIR/models/trained_models/
#possible args path,gpu,augmentation_factor 
gpu=1
aug_factor=200
#below are constant model paths; 
cnn_detector_path=$MAIN_DIR/models/mmod_human_face_detector.dat
predictor_path=$MAIN_DIR/models/shape_predictor_68_face_landmarks.dat
face_rec_model_path=$MAIN_DIR/models/dlib_face_recognition_resnet_model_v1.dat

#create directories if not prsent
ls $DATABASE_DIR/frames
if [ $? != 0 ]
then
    mkdir -p $DATABASE_DIR/frames
fi

ls $DATABASE_DIR/frames_aug
if [ $? != 0 ]
then
    mkdir -p $DATABASE_DIR/frames_aug
fi

ls $DATABASE_DIR/cropped
if [ $? != 0 ]
then
    mkdir -p $DATABASE_DIR/cropped
fi

ls $DATABASE_DIR/database
if [ $? != 0 ]
then
    mkdir -p $DATABASE_DIR/database
fi

FRAME_DIR=$DATABASE_DIR/frames
FRAME_AUG_DIR=$DATABASE_DIR/frames_aug
CROP_DIR=$DATABASE_DIR/cropped
DATABASE=$DATABASE_DIR/database/

source activate py27 && python $CODE_DIR/record_video.py $FRAME_DIR
code=`echo $?`
if [ $code == 0 ]
then
    fsf_info "Video Data Collection Successful"
    source activate py27 && python $CODE_DIR/image_augmentation.py $FRAME_DIR $FRAME_AUG_DIR $aug_factor
    cd $FACE_POS_DIR
    CUDA_VISIBLE_DEVICES=$gpu source activate py27 && python main_fpn.py $FRAME_AUG_DIR/ $cnn_detector_path
    if [ $? != 0 ]
    then
        fsf_err "Frame Augmentaion Failed"
    else
	      fsf_info "Frame Augmentaion Successful"
	      CUDA_VISIBLE_DEVICES=$gpu source activate py27 && python $CODE_DIR/fractal_face_cropper.py $FRAME_AUG_DIR/ $cnn_detector_path $CROP_DIR/
	      if [ $? == 0 ]
	      then
           fsf_info "Frame Cropping Successful"
	         source activate py27 && python $CODE_DIR/face_encode.py $CROP_DIR/ $predictor_path $face_rec_model_path $encoding_dir
           if [ $? == 0 ]
           then
            fsf_info "Embeddings created successfully"
            source activate py27 && python $CODE_DIR/train_data.py $encoding_dir $trained_model_dir
            if [ $? == 0 ]
            then
              fsf_info "Model trained scuccessfully"
            else
              fsf_err "Model training Failed"
            fi
           else
            fsf_err "Embeddings creation Failed"
           fi
        else
           fsf_err "Frame Cropping Failed"
        fi
    fi
elif [ $code == 100 ]
then
   fsf_warn "No Data Recorded"
else
   fsf_err "Video Data Collection Failed!"
fi

#remove files in frames_aug,cropped folders
#move data from frames to database
mv $FRAME_DIR/* $DATABASE
rm -rf $FRAME_AUG_DIR/*
rm -rf $CROP_DIR/*
