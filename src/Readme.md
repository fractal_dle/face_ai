# Filename: incremental_training.sh

###
#Requirements: 
#	Python 2.7 (All codes are run in Python 2.7)
#	Directory Structure
# 	CODE_DIR should have following files:
#		record_video.py
#		image_augmentation.py
#		fractal_face_cropper.py
#		Face-Pose-Net directory
#		face_encode.py
#		train_data.py
#	MAIN_DIR should have following directory structure:
#	1.	/models 
#			/mmod_human_face_detector.dat
#			/shape_predictor_68_face_landmarks.dat
#			/dlib_face_recognition_resnet_model_v1.dat
#			/trained_models/
#			/database/
#	2.	/FacerecImages
#			/frames
#			/frames_aug
#			/cropped
#			/database
###

# Runs following files in procedure

####
# Filename: record_video.py
# Location: /home/rajneesh/DLProjects/EC_Facerec/OnlineTraining/record_video.py
# Function: Takes video data of one/multiple users and extracts 100 frames
# Args: Frame directory - location to save extracted images
####

####
# Filename: image_augmentation.py
# Location: /home/rajneesh/DLProjects/EC_Facerec/OnlineTraining/image_augmentation.py
# Function: Augments images according to provided augmentation factor
# Args: FRAME_DIR - location of saved images to be augmented
#		FRAME_AUG_DIR - output directory of augmented images (Better to keep it separated 
#						from Frame directory to have original data intact till whole process completes)
#		aug_factor - # total output images 
####

####
# Filename: main_fpn.py
# Location: /home/rajneesh/DLProjects/EC_Facerec/OnlineTraining/Face-Pose-Net/main_fpn.py
# Function: Creates poses and augments the data
# Args: FRAME_AUG_DIR - location of images to be processed 
#		cnn_detector_path
####

####
# Filename: fractal_face_cropper.py
# Location: /home/rajneesh/DLProjects/EC_Facerec/OnlineTraining/fractal_face_cropper.py
# Function: Crop the images present in /frames_aug and store in /cropped folder
# Args: MAIN_DIR
#		cnn_detector_path
####

####
# Filename: face_encode.py
# Location: /home/rajneesh/DLProjects/EC_Facerec/OnlineTraining/face_encode.py
# Function: Creates encodings for all image folders present in /cropped folder
#			Creates new encoding file by appending old encodings with new ones
#			(Encoding files are stored in /home/rajneesh/DLProjects/Facerecognition/encodings)
# Args: MAIN_DIR
#		predictor_path
#		face_rec_model_path
#		encoding_dir
####

####
# Filename: train_data.py
# Location: /home/rajneesh/DLProjects/EC_Facerec/OnlineTraining/train_data.py
# Function: Trains model for latest encoding file present in Encoding dir
#			Model file is stored in /home/rajneesh/DLProjects/Facerecognition/trained_models
#			Model filename follows nomenclature as encoding file
# Args: MAIN_DIR
#		encoding_dir
#		trained_model_dir
####

# Moves data from /frames folder to database folder
# Removes data from /frames_aug and /cropped folders

# Nomenclature of encoding file-
YYYY-MM-DD_<version on same day>_face_encoding_<# of users>_<# samples>.csv
Eg- 2018-06-15_2_face_encoding_212_500.csv

# Nomenclature of encoding file-
YYYY-MM-DD_<version on same day>_model_<# of users>_<# samples>.pkl
Eg- 2018-06-15_2_model_212_500.pkl

#Trained models stored location-
/home/rajneesh/DLProjects/Facerecognition/trained_models

Encodings stored location-
/home/rajneesh/DLProjects/Facerecognition/encodings


###
# Filename : update_model_for_resigned.py
# Location : :/home/rajneesh/DLProjects/EC_Facerec/OnlineTraining/
# Independent file to update model by removing resigned employees
###
