import os,sys,errno,cv2
from termcolor import colored
import time
from datetime import datetime

spath=sys.argv[1]
n_frames = 900

def save_data(spath):
	cap = cv2.VideoCapture(1)
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
	if cap.isOpened() == False:
		print('Videocapture failed')
	print "Frame Capturing About to Start"
	print colored('Adjust Camera','green')
	time.sleep(2)
	#Add code for setting user face on camera,recognising face
	num = 0
	while(num<n_frames):
		ret, frame = cap.read()
		cv2.imshow('capturing...', cv2.resize(frame, (0,0), fx=0.5, fy=0.5))
		if(num%9==0):
			t = str(datetime.now())
			t = t.split(' ')
			t = '_'.join(t)
			cv2.imwrite(spath+'/{}frame{}.jpg'.format(t,num), frame)
		num=num+1
		
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
			
	cap.release()
	cv2.destroyAllWindows()

def make_datadir(dir_name):
	sp = spath+'/'+dir_name
	try:
		os.makedirs(sp)
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise
		else:
			pass
	return sp

def get_name():
	reply=raw_input("Hi,Are you Fractal employee?\nPlease enter Y/N\n")
	time.sleep(1)
	cnt=0
	while True:
		if reply.upper()=='Y':
			empid = raw_input('Please enter your employee id:\n')
			fname = raw_input('Please enter your first name:\n')
			lname = raw_input('Please enter your last  name:\n')
			dir_name = empid+'_'+fname+'_'+lname
			return dir_name
		elif reply.upper()=='N':
			empid='F00000'
			fname = raw_input('Please enter your first name:\n')
			lname = raw_input('Please enter your last  name:\n')
			dir_name = empid+'_'+fname+'_'+lname
			return dir_name
			#print "This system takes Fractal employee data only. Thanks for your time."
			#sys.exit(1)
		else:
			cnt=cnt+1
			if cnt==3:
				print "Number of attempts exceeded."
				return ''
		 	print colored("Wrong input entered. Enter Y/N","yellow")
			reply=raw_input("Are you Fractal employee? \n Please enter Y/N\n")


def main():
	record = raw_input("Press R to record face video\n")
	entry='M'
	dir_name=[]
	if record.upper()=='R':
		while entry.upper()=='M':
			print('Follow instructions\n')
			dir=get_name()
			if dir!='':
				dir_name.append(dir)
				a = make_datadir(dir)
				print('created dir at '+a)
				save_data(a)
			entry=='-'
			entry=raw_input(colored("If you want to add data for more users Press 'M' else press any key\n","green"))
	else:
		sys.exit(100)
	if dir_name:
		print colored('Video recorded for '+str(len(dir_name))+' users\n','green')
	else:
		print colored("Wrong Input Entered", 'red')
		sys.exit(100)

if __name__=="__main__":
	main()