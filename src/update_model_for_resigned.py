import numpy as np
import pandas as pd
import requests
import json
import csv
import os
from datetime import datetime


import pickle,os,csv,sys
from datetime import datetime
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
###
## encoding filename format YYYY-mm-dd_<version on same day>_face_encoding_<no of users>_<no of augment images>.csv
##'2018-01-01_1_face_encoding_210_500.csv'
###

converge_api_endpoint = 'https://converge.namely.com/api/v1/reports{0}'
encoding_dir='/home/rajneesh/DLProjects/Facerecognition/encodings/'
main_dir='/home/rajneesh/DLProjects/EC_Facerec/OnlineTraining/'
model_dir='/home/rajneesh/DLProjects/Facerecognition/trained_models/'
## 
#Function-get_converge_emp
#args-url (converge endpoint to pull data)
#returns list of employee ids of active employees
#
def get_converge_emp(url):
    headers = { 'ID' : 'de54bc2c-daf1-4d26-908d-734899b79597',
              'Authorization' : 'Bearer o9rAPeLCcLnhc8HLVShLJNB9mIL1mvgx3vnHnnftuHDqIL0jZDRo1K5DdSRUnXTs',
              'Accept' : 'application/json'}
  
    r = requests.get(url, headers = headers)
    reports=r.json()['reports']
    reports=[i['content'] for i in reports]
    if (r.status_code == 200):
    	return [j[1].lower() for j in reports[0] if j[1] not in ["fAdmin Helpdesk","IT Helpdesk","None","USVISA","TRVISA","TRDESK","HRMIS","FRABOA","FRACTAL HC","MARKETING","FAA","BREAKFREE","","MISUSER","FREXEC",None]]
    	#return [(j[1]+'_'+'_'.join(j[2].split(' '))).lower() for j in reports[0] if j[1] not in ["fAdmin Helpdesk","IT Helpdesk","None","USVISA","TRVISA","TRDESK","HRMIS","FRABOA","FRACTAL HC","MARKETING","FAA","BREAKFREE","","MISUSER","FREXEC",None]]

def get_encoding_emp(encoding_path):
	encoding=pd.read_csv(encoding_path)
	dic={}
	lst=[]
	for item in encoding['label'].drop_duplicates().tolist():
		a=item.split('_')[0].lower()
		dic[a]=item
		lst.append(a)
	return dic,lst
	#return [item.lower() for item in encoding['label'].drop_duplicates().tolist()]

def list_to_csv(employee_data,file):
    with open(file, "wb") as f:
        writer = csv.writer(f, lineterminator='\n')
        for val in employee_data:
        	writer.writerow([val])

def update_encodings(prev_file,update_file,lst):
	df=pd.read_csv(prev_file)
	df[~df['label'].isin(lst)].to_csv(update_file, sep=',')

def get_latest_encoding_filename():
	min=1000
	day_tag=0
	version=-100
	cur_date=int(''.join(datetime.now().date().isoformat().split('-')))
	for i in os.listdir(encoding_dir):
		temp= cur_date- int(''.join(i.split('_')[0].split('-')))
		v=i.split('_')[1]
		if temp<min:
			min=temp
			file=i
			version=v
		elif temp==min:
			day_tag=1
			if version < v:
				file=i
				version=v
	return file,day_tag,version

def get_new_encoding_filename(file,remove_count,day_tag,version):
	fname=file.split('_')
	fname[0]=datetime.now().date().isoformat()
	fname[4]=str(int(fname[4])-remove_count)
	if day_tag==1:
		fname[1]=str(int(version)+1)
	else:
		fname[1]='1'
	#print '_'.join(fname)
	return '_'.join(fname)

def train_ml_model(df, model_file):	
	y = df['label']
	X = df.drop('label',  axis=1)
	#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
	print "classes",len([name for name in set(y)])
	print("\nFitting the classifier to the training set")
	svm = SVC(kernel='linear', C=10, gamma=0.001, probability=True)
	svm.fit(X, y)
	# Create a list of class names
	#class_names = [name for name in set(y)]
	#print classification_report(y, y_pred, target_names=class_names)
	#print confusion_matrix(y, y_pred, labels=range(len(class_names)))
	with open(model_file, 'wb') as fp:
		pickle.dump(svm, fp)
		
def main():
	active_mumbai=converge_api_endpoint.format('/ef626569-e636-4030-abd6-6421d17c8ba8')
	encoding_file,day_tag,version=get_latest_encoding_filename()
	converge_emp=get_converge_emp(active_mumbai)
	dict_encoding,encoding_emp=get_encoding_emp(encoding_dir+encoding_file)

	#users resigned or moved to different location (not in mumbai)
	not_in_mumbai=list(set(encoding_emp)-set(converge_emp))
	#list_to_csv(encoding_emp,'encod.csv')
	#list_to_csv(converge_emp,'convrg.csv')
	not_in_mumbai=[dict_encoding[x] for x in not_in_mumbai]
	print not_in_mumbai
	if not_in_mumbai:
		new_encoding_file=get_new_encoding_filename(encoding_dir+encoding_file,len(not_in_mumbai),day_tag,version)
		update_encodings(encoding_dir+encoding_file,encoding_dir+new_encoding_file,not_in_mumbai)
		model_save_path=new_encoding_file.split('.')[0].split('_')
		model_save_path = model_dir+model_save_path[0]+'_'+model_save_path[1]+'_model_'+model_save_path[4]+'_'+model_save_path[5]+'.pkl'
		print model_save_path
		df_all=pd.read_csv(encoding_dir+new_encoding_file)
		#check shape here if (..,129) then proceed
		df_all=df_all.iloc[:,1:]
		print df_all.head(1)
		train_ml_model(df_all, model_save_path)

	

if __name__ == "__main__":
	main()


#Rename List
#Old New
#f02573_abhiram_elangovan f02573_abhiram_e
#f00288_tejas_sanghvi f00288_tejas_sanghavi
#f00523_naushad_sheikh f00523_naushad_shaikh
#f00875_sandeep_bhograju f00875_sandeep_bhogaraju
#f01142_tanvi _khanna f01142_tanvi_khanna
#f00213_rajeswari_aradhyula f00213_raj_aradhyula
#f02409_prakash_vanapalli f02409_vanapalli_prakash
#f01752_kaushik_choudhary f01752_kaushik_chowdhury
#f00614_aditi_singh f00614_aditi_agrawal_singh
#f02560_hitendra _jharwal f02560_hitendra_jharwal
#f01653_mohdsofyan_shaikh f01653_mohd_sofyan_shaikh
#f01702_anil_vannela f01702_anilkumar_vannela
#f02677_shubham_pachori f02677_shubham_pachori_pachori
#f02295_sachin_chandra f02295_sachin_bonagiri
#f00328_aliasgar_rajkotwaa f00328_aliasgar_rajkotwala
#f02367_nithish_kumar f02367_nithish_reddy
#f02120_onish_ f02120_onish_chamoli

#Resign
#f01925_surabhi_pawar
#f02240_vaibhav_anday
#f02410_abhishek_yadav
#f02270_rahhel_kadri