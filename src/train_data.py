import pickle,os,csv,sys
from datetime import datetime
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

def train_ml_model(df, model_file):	
	y = df['label']
	X = df.drop('label',  axis=1)
	#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
	print "classes",len([name for name in set(y)])
	print("\nFitting the classifier to the training set")
	svm = SVC(kernel='linear', C=10, gamma=0.001, probability=True)
	svm.fit(X, y)
	# Create a list of class names
	#class_names = [name for name in set(y)]
	#print classification_report(y, y_pred, target_names=class_names)
	#print confusion_matrix(y, y_pred, labels=range(len(class_names)))
	with open(model_file, 'wb') as fp:
		pickle.dump(svm, fp)

def get_latest_encoding_filename(encoding_dir):
	min=1000
	day_tag=0
	version=-100
	cur_date=int(''.join(datetime.now().date().isoformat().split('-')))
	for i in os.listdir(encoding_dir):
		temp= cur_date- int(''.join(i.split('_')[0].split('-')))
		v=i.split('_')[1]
		if temp<min:
			min=temp
			file=i
			version=v
		elif temp==min:
			day_tag=1
			if version < v:
				file=i
				version=v
	return file,day_tag,version

if __name__=="__main__":
	#main_dir=sys.argv[1]
	encoding_dir=sys.argv[1]
	model_dir=sys.argv[2]
	file,day_tag,version=get_latest_encoding_filename(encoding_dir)
	model_save_path=file.split('.')[0].split('_')
	model_save_path = model_dir+model_save_path[0]+'_'+model_save_path[1]+'_model_'+model_save_path[4]+'_'+model_save_path[5]+'.pkl'
	print model_save_path
	df_all=pd.read_csv(encoding_dir+file)
	df_all=df_all.iloc[:,1:]
	train_ml_model(df_all, model_save_path)
