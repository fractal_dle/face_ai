import cv2
import dlib
from PIL import Image
import time
import numpy as np

save_path = '/home/fractaluser/FractalDL/Facerecognition/images/video_captured_images/'
save_path2 = '/home/fractaluser/FractalDL/Facerecognition/images/video_captured_images_raw/'

detector = dlib.get_frontal_face_detector()
cnn_face_detector = dlib.cnn_face_detection_model_v1('../models/mmod_human_face_detector.dat')
video_capture = cv2.VideoCapture('rtmp://172.16.7.201/live/test')
process_this_frame = True
margin = 5
i = 0

while(True):

	ret, frame = video_capture.read()
	#print ret, frame.shape, 
	if not ret:
		continue

	i += 1	
	#if process_this_frame:
	if i%5 == 1:

		frame = cv2.resize(frame, (0,0), fx = 0.5, fy = 0.5, interpolation = cv2.INTER_CUBIC)
		#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		blur = cv2.Laplacian(frame, cv2.CV_64F).var()
		if blur <=80:
			continue

		dets = cnn_face_detector(frame, 1)
		
		if len(dets) <= 0:
			continue

		#print "Good Frame"
		height, width, _ = frame.shape
		for k, d in enumerate(dets):
			#left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
			left, top, right, bottom = 	d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()

			left = max(0, left-margin)
			top = max(0, top - margin)
			right = min(right+margin, width)
			bottom = min(bottom+margin, height)

			img = frame[top:bottom, left:right]
			time_in_millis = int(round(time.time() * 1000))

			img = cv2.resize(img, (0,0), fx = 2, fy = 2, interpolation = cv2.INTER_AREA)				
			cv2.imwrite(save_path+str(time_in_millis)+'.jpg', img)

			cv2.imwrite(save_path2+str(time_in_millis)+'.jpg', frame)

	else:
		pass
		
	#cv2.imshow('FaceDetection', frame)
	#process_this_frame = not process_this_frame
	if cv2.waitKey(10) & 0xFF == ord('q'):
			break


