#!/usr/bin/python

import sys
import os
import dlib
import pickle
import cv2
import random
import time
from PIL import Image

import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import SGDClassifier

def train_ml_model(df, model_file):
	
	y = df['label']
	X = df.drop('label',  axis=1)

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=42)
	
	st_time = time.time()
	print("\nFitting the classifier to the training set")
	
	# Train SVM classifier
	#param_grid = {'kernel':['linear'], 'C': [10, 100], 'gamma': [0.001, 0.01]}
	#svm = GridSearchCV(SVC(class_weight='balanced', probability=True), param_grid=param_grid, n_jobs=-1, cv=10)
	#svm = SVC(kernel='linear', probability=True)

	clf = SGDClassifier()
	clf.partial_fit(X_train, y_train,classes=np.unique(y_train))
	#clf.fit(X_train, y_train)
	#svm = SVC(kernel='linear', C=10, gamma=0.001, probability=True)
	#svm.fit(X_train, y_train)		

	# print "\nBest estimator of SVM by grid search:"
	# print svm.best_params_ # {'kernel': 'linear', 'C': 10, 'gamma': 0.001}

	#print 'Time in classification {0: .2f} seconds'.format((ed_time-st_time)/1000)
	print 'Time in classification'
	print time.time()-st_time
	print("\nPredicting people's names on the test set")
	y_pred = clf.predict(X_test)

	# Create a list of class names
	class_names = [name for name in set(y)]
	print classification_report(y_test, y_pred, target_names=class_names)
	#print confusion_matrix(y_test, y_pred, labels=range(len(class_names)))

	# Now train on full dataset
	print("\nTraining on full dataset")
	clf.partial_fit(X, y)
	
	with open(model_file, 'wb') as fp:
		pickle.dump(clf, fp)

def _main():
	
	# Initialize dlib shape predictor
	predictor_path = '/home/rajneesh/DLProjects/Facerecognition/models/shape_predictor_68_face_landmarks.dat'
	shape_predictor = dlib.shape_predictor(predictor_path)

	# Initialize cnn face detector
	face_rec_model_path = '/home/rajneesh/DLProjects/Facerecognition/models/dlib_face_recognition_resnet_model_v1.dat'
	facerec = dlib.face_recognition_model_v1(face_rec_model_path)

	# Read images from image_directory
	parent_dir_path = '/data/FacerecImages/face_rec_data/sample_500_images/'
	model_save_path = '../models/model_active_210_500_test_SGD.pkl'
	encodings_file = 'face_encodings_active_210_500.csv'
	input_dim = 128


	if not os.path.exists(encodings_file):

		X = []
		y = []

		#create empty dataframe
		column_list = list(range(input_dim))
		column_list.append('label')
		df = pd.DataFrame(columns=column_list)
		i = 0
		
		for child_dir in os.listdir(parent_dir_path):
			label = child_dir
			print "Processing: ", label

			for image_name in os.listdir(parent_dir_path+child_dir):
				image_path = parent_dir_path+child_dir+'/'+image_name
				
				try:
					cropped_image = cv2.imread(image_path)
					width, height, _ = cropped_image.shape
					
					dd = dlib.rectangle(0, 0, width, height)
					shape = shape_predictor(cropped_image, dd)

					# Compute face encoding (128D vectors)
					face_descriptor = list(facerec.compute_face_descriptor(cropped_image, shape))
					face_descriptor.append(label)
					df.loc[i] = face_descriptor
					i += 1

				except:
					print image_path
					#os.remove(image_path)
					continue	

		df.to_csv(encodings_file, encoding='utf-8', index=False)
	else:
		#Read esisting embeding file
		df = pd.read_csv(encodings_file)

	# train ML model
	train_ml_model(df, model_save_path)

if __name__ == '__main__':
	start = time.time()
	_main()
	print time.time()-start ," seconds"

