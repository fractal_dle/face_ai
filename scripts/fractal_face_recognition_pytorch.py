#!/usr/bin/python

import sys
import os
import dlib
import glob
from skimage import io
import pickle
import cv2
import numpy as np
import colorsys
import pickle
import csv
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import random
import time

import torch
from torch.autograd import Variable
import torch.nn.functional as F
from face_recognizer_feedforward_pytorch import FeedforwardNeuralNetModel

def face_prediction(model, face_encoding, PROB_THRESH = .5):

	face_encoding = torch.FloatTensor(face_encoding)
	if torch.cuda.is_available():		
		face_encoding = Variable(face_encoding.cuda())
	else:
		face_encoding = Variable(face_encoding)

	face_encoding = face_encoding.float().unsqueeze(0)

	# pass it through the model
	prediction = model(face_encoding)
	h_x = torch.mean(F.softmax(prediction, 1), dim=0).data
	probs, idx = h_x.sort(0, True)

	probs_arr = probs.cpu().numpy()
	idx_arr = idx.cpu().numpy()
	
	print (probs_arr[0], idx_arr[0])
	return (probs_arr[0], idx_arr[0])


def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def _main():

	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()
	print "Loaded OpenCV Detector"

	# Initialize dlib shape predictor
	predictor_path = '/home/rajneesh/DLProjects/Facerecognition/models/shape_predictor_68_face_landmarks.dat'
	shape_predictor = dlib.shape_predictor(predictor_path)
	print "Loaded CNN Predictor"

	# Initialize cnn face detector
	face_rec_model_path = '/home/rajneesh/DLProjects/Facerecognition/models/dlib_face_recognition_resnet_model_v1.dat'
	facerec = dlib.face_recognition_model_v1(face_rec_model_path)
	print "Loaded Embedding Model"

	# Initialize cnn face detector
	cnn_face_detector_path = '/home/rajneesh/DLProjects/Facerecognition/models/mmod_human_face_detector.dat'
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detector_path)
	print "Loaded CNN Detector"
	
	# Store lableEncoder
	with open("/home/rajneesh/DLProjects/Facerecognition/models/lable_encoder.pkl", "rb") as handle:
		le = pickle.load(handle)
	print "Loaded Face Labels"

	input_dim = 128
	hidden_dim = 200
	output_dim = len(list(le.classes_))

	# Load face SVM based face predictor
	model = FeedforwardNeuralNetModel(input_dim, hidden_dim, output_dim)
	if torch.cuda.is_available():
		model.cuda()
	print "Initialized Pytorch Model"

	model_load_path='../models/pytorch_model_150.pth'	
	model.load_state_dict(torch.load(model_load_path))
	print "Loaded Pytorch Model"
	
	image_size = (720, 480)
	#fourcc = cv2.VideoWriter_fourcc('M','J','P','G') #opencv3
	#videoWriter = cv2.VideoWriter('Fractal_Face_REC.avi', fourcc, 15.0, image_size)
	
	DETECTION_MODEL = 'cnn' # or 'cnn'
	UPSAMPLE_FACTOR = 1
	PROB_THRESH = 0.90

	#video_capture = cv2.VideoCapture('http://172.16.6.116:8080/video?dummy=param.mjpg')
	video_capture = cv2.VideoCapture(0)
	video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

	process_this_frame = True
	frame_num = 0
	while(True):

		ret, frame = video_capture.read()
		#img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		height, width, _ = frame.shape

		font = ImageFont.truetype(font='/home/rajneesh/DLProjects/Utilities/font/FiraMono-Medium.otf', size=np.floor(5e-2 * height + 0.5).astype('int32'))
		thickness = (width + height) // 300

		pil_image = Image.fromarray(frame)
		draw = ImageDraw.Draw(pil_image)
		frame_num += 1
		if frame_num % 2 == 0:
		
			start_time = time.time()
			if DETECTION_MODEL == 'cnn':
				detector = cnn_face_detector			
			dets = face_detection(detector, frame, UPSAMPLE_FACTOR)

			# Now process each face we found.
			face_encodings = []
			face_locations = []
			for k, d in enumerate(dets):
				
				if DETECTION_MODEL == 'cnn':
					left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
				else:
					left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
				
				# Get the landmarks/parts for the face in box d.
				dd = dlib.rectangle(left, top, right, bottom)
				shape = shape_predictor(frame, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = facerec.compute_face_descriptor(frame, shape, 50)
				(pred_probablity, face_id)  = face_prediction(model, face_descriptor, PROB_THRESH)
				
				if not pred_probablity > PROB_THRESH:
					continue 				

				predicted_face = le.inverse_transform(face_id)
				label = '{} {:.2f}'.format(predicted_face, pred_probablity)
				#label = predicted_face
				label_size = draw.textsize(label, font)
				label_w, label_h = label_size

				for i in range(thickness):
					draw.rectangle([left + i, top + i, right - i, bottom - i], outline=(255, 255, 255))
									
				new_bottom = min(bottom+label_h+2, height-2)
				label_origin_bottom = new_bottom-label_h-2

				if label_w < (right - left):
					label_w = (right - left)

				draw.rectangle([left, label_origin_bottom, left+label_w, new_bottom], fill=(0, 0, 150))					
				draw.text(np.array([left, bottom - 2]), label, fill=(255, 255, 255), font=font)
				

			end_time = time.time()
			print "Processing Time in MS: {}".format(int((end_time - start_time)*1000))

		img_display = cv2.resize(np.array(pil_image), image_size)
		cv2.imshow('FaceRecognition', np.array(img_display))
		#videoWriter.write(img_display)        				

		#process_this_frame = not process_this_frame
		if cv2.waitKey(10) & 0xFF == ord('q'):
			break

if __name__ == '__main__':
	_main()

