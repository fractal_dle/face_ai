#!/usr/bin/python

import sys
import os
import dlib
import glob
from skimage import io
import pickle
import cv2
import numpy as np
import colorsys
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import random

def face_encoder(detector, DETECTION_MODEL, UPSAMPLE_FACTOR, shape_predictor, facerec_model):
	dir_path = '/home/rajneesh/DLProjects/LiveDemo/clusters/'
	know_face_encoding = []
	known_faces = []
	
	all_dirs = os.listdir(dir_path)
	for each_dir in all_dirs:
		all_images = os.listdir(dir_path+each_dir)
		name = each_dir
		print "processing:", name
		for each_image in all_images:
			image_path  = dir_path+each_dir+'/'+each_image
			image = cv2.imread(image_path)		
			
			'''
			dets = face_detector(detector, image, UPSAMPLE_FACTOR)		
			for d in dets:
				if DETECTION_MODEL == 'cnn':
					left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
				else:
					left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
				
				# Get the landmarks/parts for the face in box d.
				dd = dlib.rectangle(left, top, right, bottom)
				shape = shape_predictor(image, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = facerec_model.compute_face_descriptor(image, shape)
				know_face_encoding.append(face_descriptor)
				known_faces.append(name)
			'''
			height, width, _ = image.shape
			dd = dlib.rectangle(0, 0, width, height)
			shape = shape_predictor(image, dd)

			# Compute face encoding (128D vectors)
			face_descriptor = facerec_model.compute_face_descriptor(image, shape)
			know_face_encoding.append(face_descriptor)
			known_faces.append(name)

	
	#print type(know_face_encoding), len(know_face_encoding), know_face_encoding[0]
	return (know_face_encoding, known_faces)
		

def face_detector(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def _main():
	
	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	cnn_face_detector_path = '../models/mmod_human_face_detector.dat'
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detector_path)

	# Initialize dlib shape predictor
	predictor_path = '../models/shape_predictor_68_face_landmarks.dat'
	shape_predictor = dlib.shape_predictor(predictor_path)

	# Initialize cnn face detector
	face_rec_model_path = '../models/dlib_face_recognition_resnet_model_v1.dat'
	facerec_model = dlib.face_recognition_model_v1(face_rec_model_path)

	DETECTION_MODEL = 'cnn' # or 'cnn'
	UPSAMPLE_FACTOR = 1
	PROB_THRESH = 0.65
	
	if DETECTION_MODEL == 'cnn':
		detector = cnn_face_detector
				
	# Get face encodings
	(known_face_encodings, known_faces) = face_encoder(detector, DETECTION_MODEL, UPSAMPLE_FACTOR, shape_predictor, facerec_model)

	video_capture = cv2.VideoCapture('/home/rajneesh/DLProjects/LiveDemo/videos/00045.MTS')
	#video_capture = cv2.VideoCapture(0)
	process_this_frame = True


	image_size = (960, 540)
	fourcc = cv2.VideoWriter_fourcc('M','J','P','G') #opencv3
	videoWriter = cv2.VideoWriter('/home/rajneesh/DLProjects/LiveDemo/videos/Banglore_facerec.avi', fourcc, 25.0, image_size)
	
	print "Starting Video: ...."
	while(True):
		ret, frame = video_capture.read()
		#img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		#frame = cv2.resize(frame, (0,0), fx=0.5, fy=0.5) 
		height, width, _ = frame.shape

		font = ImageFont.truetype(font='/home/rajneesh/DLProjects/Utilities/font/FiraMono-Medium.otf', size=np.floor(5e-2 * height + 0.5).astype('int32'))
		thickness = (width + height) // 300

		if process_this_frame:							
			dets = face_detector(detector, frame, UPSAMPLE_FACTOR)

			# Now process each face we found.
			face_encodings = []
			face_locations = []
			for k, d in enumerate(dets):
				
				#print "Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(k, d.left(), d.top(), d.right(), d.bottom())				
				#print "Detection {}: Left: {} Top: {} Right: {} Bottom: {} Confidence: {}".format(i, d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom(), d.confidence)
				if DETECTION_MODEL == 'cnn':
					left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
				else:
					left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()

				print bottom - top,right - left
				if bottom - top < 94 and right - left < 94:
					continue
				
				# Get the landmarks/parts for the face in box d.
				dd = dlib.rectangle(left, top, right, bottom)
				shape = shape_predictor(frame, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = facerec_model.compute_face_descriptor(frame, shape)
				face_encodings.append(face_descriptor)
				face_locations.append((left, top, right, bottom)) 				
			
			pil_image = Image.fromarray(frame)
			draw = ImageDraw.Draw(pil_image)
			
			# Plot face recognition
			for i, face_encoding in enumerate(face_encodings):
				
				dist = [np.linalg.norm(np.array(known_face_encoding)-np.array(face_encoding)) for known_face_encoding in known_face_encodings]
				
				min_dist = min(dist)
				#print min_dist, dist.index(min_dist)
				if min_dist < PROB_THRESH:
					name = known_faces[dist.index(min_dist)]
				else:
					name = 'Others'
					
				#print min_dist, dist.index(min(dist)), known_faces[dist.index(min_dist)], '\n', dist
				left, top, right, bottom = face_locations[i][0], face_locations[i][1], face_locations[i][2], face_locations[i][3]

				#label = '{} {:.2f}'.format(name, min_dist)
				label = name
				label_size = draw.textsize(label, font)
				label_w, label_h = label_size
				#print np.floor(4e-2 * height + 0.5), thickness

				for i in range(thickness):
					draw.rectangle([left + i, top + i, right - i, bottom - i], outline=(255, 255, 255))
									
				new_bottom = min(bottom+label_h+2, height-2)
				label_origin_bottom = new_bottom-label_h-2
				if label_w < right-left:
					label_w = right-left
				else:
					left+label_w+2
					
				draw.rectangle([left, label_origin_bottom, left+label_w+2, new_bottom], fill=(0, 0, 150))					
				draw.text(np.array([left, bottom - 2]), label, fill=(255, 255, 255), font=font)

				del dist
				
			img_display = cv2.resize(np.array(pil_image), image_size)
			#cv2.imshow("FaceRecognition", cv2.resize(save_image, (3840, 2160)))
			cv2.imshow("ActionDetection", img_display)
			videoWriter.write(img_display)        			

		process_this_frame = process_this_frame
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

	videoWriter.release()
	#videoWriter.release()
	cv2.destroyAllWindows()

if __name__ == '__main__':
	_main()

