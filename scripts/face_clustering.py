#!/usr/bin/python
# The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
#
#   This example shows how to use dlib's face recognition tool for clustering using chinese_whispers.
#   This is useful when you have a collection of photographs which you know are linked to
#   a particular person, but the person may be photographed with multiple other people.
#   In this example, we assume the largest cluster will contain photos of the common person in the
#   collection of photographs. Then, we save extracted images of the face in the largest cluster in
#   a 150x150 px format which is suitable for jittering and loading to perform metric learning (as shown
#   in the dnn_metric_learning_on_images_ex.cpp example.
#   https://github.com/davisking/dlib/blob/master/examples/dnn_metric_learning_on_images_ex.cpp
#
# COMPILING/INSTALLING THE DLIB PYTHON INTERFACE
#   You can install dlib using the command:
#       pip install dlib
#
#   Alternatively, if you want to compile dlib yourself then go into the dlib
#   root folder and run:
#       python setup.py install
#   or
#       python setup.py install --yes USE_AVX_INSTRUCTIONS
#   if you have a CPU that supports AVX instructions, since this makes some
#   things run faster.  This code will also use CUDA if you have CUDA and cuDNN
#   installed.
#
#   Compiling dlib should work on any operating system so long as you have
#   CMake and boost-python installed.  On Ubuntu, this can be done easily by
#   running the command:
#       sudo apt-get install libboost-python-dev cmake
#
#   Also note that this example requires scikit-image which can be installed
#   via the command:
#       pip install scikit-image
#   Or downloaded from http://scikit-image.org/download.html. 

import sys
import os
import dlib
import glob
import cv2
from skimage import io

predictor_path = '/home/rajneesh/DLProjects/Facerecognition/models/shape_predictor_5_face_landmarks.dat'
face_rec_model_path = '/home/rajneesh/DLProjects/Facerecognition/models/dlib_face_recognition_resnet_model_v1.dat'
faces_folder_path = '/home/rajneesh/DLProjects/LiveDemo/frames_aug'
output_folder = '/home/rajneesh/DLProjects/LiveDemo/clusters/'

detector = dlib.get_frontal_face_detector()
sp = dlib.shape_predictor(predictor_path)
facerec = dlib.face_recognition_model_v1(face_rec_model_path)

descriptors = []
images = []

# Now find all the faces and compute 128D face descriptors for each face.
for f in glob.glob(os.path.join(faces_folder_path, "*")):
	#print("Processing file: {}".format(f))
	img = io.imread(f)
	width, height, _ = img.shape
	left, top, right, bottom = 	2, 2, height, width
	# Get the landmarks/parts for the face in box d.
	shape = sp(img, dlib.rectangle(left, top, right, bottom))

	# Compute the 128D vector that describes the face in img identified by shape.  
	face_descriptor = facerec.compute_face_descriptor(img, shape)
	descriptors.append(face_descriptor)
	images.append((img, shape))

# Now let's cluster the faces.  
labels = dlib.chinese_whispers_clustering(descriptors, 0.40)
num_classes = len(set(labels))
print("Number of clusters: {}".format(num_classes))
print "len: ", len(images)

for i, image in enumerate(images):
	# Ensure output directory exists
	output_folder_path = output_folder+str(labels[i])
	if not os.path.isdir(output_folder_path):
		os.makedirs(output_folder_path)

	file_path = output_folder_path+"/face_" +str(i)
	# The size and padding arguments are optional with default size=150x150 and padding=0.25
	img, shape = image
	img = cv2.resize(cv2.cvtColor(img, cv2.COLOR_RGB2BGR), (0,0), fx = 2, fy = 2, interpolation = cv2.INTER_CUBIC)
	cv2.imwrite(output_folder_path+"/face_" +str(i)+".jpg", img)