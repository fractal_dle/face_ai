import dlib
from collections import OrderedDict
import numpy as np
import cv2

# import the necessary packages
import numpy as np
import cv2
import matplotlib.pyplot as plt

def rect_to_bb(rect):
	# take a bounding predicted by dlib and convert it to the format (x, y, w, h) as we would normally do with OpenCV
	x = rect.left()
	y = rect.top()
	w = rect.right() - x
	h = rect.bottom() - y

	# return a tuple of (x, y, w, h)
	return (x, y, w, h)

def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((68, 2), dtype=dtype)

	# loop over the 68 facial landmarks and convert them to a 2-tuple of (x, y)-coordinates
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)

	# return the list of (x, y)-coordinates
	return coords

def visualize_facial_landmarks(image, shape, colors=None, alpha=0.75):
	# create two copies of the input image -- one for the overlay and one for the final output image
	overlay = image.copy()
	output = image.copy()

	# if the colors list is None, initialize it with a unique color for each facial landmark region
	if colors is None:
		colors = [(19, 199, 109), (79, 76, 240), (230, 159, 23),
			(168, 100, 168), (158, 163, 32),
			(163, 38, 32), (180, 42, 220)]

	# loop over the facial landmark regions individually
	for (i, name) in enumerate(FACIAL_LANDMARKS_IDXS.keys()):
		# grab the (x, y)-coordinates associated with the face landmark
		(j, k) = FACIAL_LANDMARKS_IDXS[name]
		pts = shape[j:k]

		# check if are supposed to draw the jawline
		if name == "jaw":
			# since the jawline is a non-enclosed facial region, just draw lines between the (x, y)-coordinates
			for l in range(1, len(pts)):
				ptA = tuple(pts[l - 1])
				ptB = tuple(pts[l])
				cv2.line(overlay, ptA, ptB, colors[i], 2)

		# otherwise, compute the convex hull of the facial landmark coordinates points and display it
		else:
			hull = cv2.convexHull(pts)
			cv2.drawContours(overlay, [hull], -1, colors[i], -1)

	# apply the transparent overlay
	cv2.addWeighted(overlay, alpha, output, 1 - alpha, 0, output)

	# return the output image
	return output

def align(predictor, image, gray, rect):
	# convert the landmark (x, y)-coordinates to a NumPy array
	desiredLeftEye=(0.35, 0.35)
	desiredFaceWidth=256
	desiredFaceHeight=None

	#if the desired face height is None, set it to be the desired face width (normal behavior)
	if desiredFaceHeight is None:
		desiredFaceHeight = desiredFaceWidth
	
	shape = predictor(gray, rect)
	shape = shape_to_np(shape)

	# extract the left and right eye (x, y)-coordinates
	(lStart, lEnd) = FACIAL_LANDMARKS_IDXS["left_eye"]
	(rStart, rEnd) = FACIAL_LANDMARKS_IDXS["right_eye"]
	leftEyePts = shape[lStart:lEnd]
	rightEyePts = shape[rStart:rEnd]

	# compute the center of mass for each eye
	leftEyeCenter = leftEyePts.mean(axis=0).astype("int")
	rightEyeCenter = rightEyePts.mean(axis=0).astype("int")

	# compute the angle between the eye centroids
	dY = rightEyeCenter[1] - leftEyeCenter[1]
	dX = rightEyeCenter[0] - leftEyeCenter[0]
	angle = np.degrees(np.arctan2(dY, dX)) - 180

	# compute the desired right eye x-coordinate based on the desired x-coordinate of the left eye
	desiredRightEyeX = 1.0 - desiredLeftEye[0]

	# determine the scale of the new resulting image by taking the ratio of the distance between eyes in the *current*
	# image to the ratio of distance between eyes in the *desired* image
	dist = np.sqrt((dX ** 2) + (dY ** 2))
	desiredDist = (desiredRightEyeX - desiredLeftEye[0])
	desiredDist *= desiredFaceWidth
	scale = desiredDist / dist

	# compute center (x, y)-coordinates (i.e., the median point) between the two eyes in the input image
	eyesCenter = ((leftEyeCenter[0] + rightEyeCenter[0]) // 2, (leftEyeCenter[1] + rightEyeCenter[1]) // 2)

	# grab the rotation matrix for rotating and scaling the face
	M = cv2.getRotationMatrix2D(eyesCenter, angle, scale)

	# update the translation component of the matrix
	tX = desiredFaceWidth * 0.5
	tY = desiredFaceHeight * desiredLeftEye[1]
	M[0, 2] += (tX - eyesCenter[0])
	M[1, 2] += (tY - eyesCenter[1])

	# apply the affine transformation
	(w, h) = (desiredFaceWidth, desiredFaceHeight)
	output = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC)

	# return the aligned face
	return output

# define a dictionary that maps the indexes of the facial landmarks to specific face regions
FACIAL_LANDMARKS_IDXS = OrderedDict([
	("mouth", (48, 68)),
	("right_eyebrow", (17, 22)),
	("left_eyebrow", (22, 27)),
	("right_eye", (36, 42)),
	("left_eye", (42, 48)),
	("nose", (27, 36)),
	("jaw", (0, 17))
])

# the facial landmark predictor
predictor_path = '/home/rajneeshkumar/dlib_dl/models/shape_predictor_68_face_landmarks.dat'
face_file_path = '/home/rajneeshkumar/dlib_dl/images/input/2017-12-15_18:39:18.338860frame143.jpg'

detector = dlib.get_frontal_face_detector()
sp = dlib.shape_predictor(predictor_path)

# Load the image using OpenCV
bgr_img = cv2.imread(face_file_path)
if bgr_img is None:
    print("Sorry, we could not load '{}' as an image".format(face_file_path))
    exit()

# Convert to RGB since dlib uses RGB images
gray_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB)
rects = detector(bgr_img, 1)

num_faces = len(rects)
if num_faces == 0:
    print("Sorry, there were no faces found in '{}'".format(face_file_path))
    exit()

for rect in rects:
	# extract the ROI of the *original* face, then align the face using facial landmarks
	(x, y, w, h) = rect_to_bb(rect)
	face_original = cv2.resize(bgr_img[y:y + h, x:x + w], (256, 256), interpolation = cv2.INTER_CUBIC)
	face_aligned = align(sp, bgr_img, gray_img, rect)

	plt.subplot(121)
	plt.imshow(face_original)		
	plt.subplot(122)
	plt.imshow(face_aligned)
	plt.show()
