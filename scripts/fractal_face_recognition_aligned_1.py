#!/usr/bin/python

import sys
import os
import dlib
import glob
from skimage import io
import pickle
import cv2
import time
import numpy as np
import colorsys
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import random
from collections import OrderedDict

# import pafy
# url = "https://www.youtube.com/watch?v=aKX8uaoy9c8"
# videoPafy = pafy.new(url)
# best = videoPafy.getbest(preftype="webm")
# video=cv2.VideoCapture(best.url)

# define a dictionary that maps the indexes of the facial landmarks to specific face regions
FACIAL_LANDMARKS_IDXS = OrderedDict([
	("mouth", (48, 68)),
	("right_eyebrow", (17, 22)),
	("left_eyebrow", (22, 27)),
	("right_eye", (36, 42)),
	("left_eye", (42, 48)),
	("nose", (27, 36)),
	("jaw", (0, 17))
])

def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((68, 2), dtype=dtype)

	# loop over the 68 facial landmarks and convert them to a 2-tuple of (x, y)-coordinates
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)

	# return the list of (x, y)-coordinates
	return coords

def align(predictor, image, gray, rect):
	# convert the landmark (x, y)-coordinates to a NumPy array
	desiredLeftEye=(0.35, 0.35)
	desiredFaceWidth=256
	desiredFaceHeight=None

	#if the desired face height is None, set it to be the desired face width (normal behavior)
	if desiredFaceHeight is None:
		desiredFaceHeight = desiredFaceWidth
	
	shape = predictor(gray, rect)
	shape = shape_to_np(shape)

	# extract the left and right eye (x, y)-coordinates
	(lStart, lEnd) = FACIAL_LANDMARKS_IDXS["left_eye"]
	(rStart, rEnd) = FACIAL_LANDMARKS_IDXS["right_eye"]
	leftEyePts = shape[lStart:lEnd]
	rightEyePts = shape[rStart:rEnd]

	# compute the center of mass for each eye
	leftEyeCenter = leftEyePts.mean(axis=0).astype("int")
	rightEyeCenter = rightEyePts.mean(axis=0).astype("int")

	# compute the angle between the eye centroids
	dY = rightEyeCenter[1] - leftEyeCenter[1]
	dX = rightEyeCenter[0] - leftEyeCenter[0]
	angle = np.degrees(np.arctan2(dY, dX)) - 180

	# compute the desired right eye x-coordinate based on the desired x-coordinate of the left eye
	desiredRightEyeX = 1.0 - desiredLeftEye[0]

	# determine the scale of the new resulting image by taking the ratio of the distance between eyes in the *current*
	# image to the ratio of distance between eyes in the *desired* image
	dist = np.sqrt((dX ** 2) + (dY ** 2))
	desiredDist = (desiredRightEyeX - desiredLeftEye[0])
	desiredDist *= desiredFaceWidth
	scale = desiredDist / dist

	# compute center (x, y)-coordinates (i.e., the median point) between the two eyes in the input image
	eyesCenter = ((leftEyeCenter[0] + rightEyeCenter[0]) // 2, (leftEyeCenter[1] + rightEyeCenter[1]) // 2)

	# grab the rotation matrix for rotating and scaling the face
	M = cv2.getRotationMatrix2D(eyesCenter, angle, scale)

	# update the translation component of the matrix
	tX = desiredFaceWidth * 0.5
	tY = desiredFaceHeight * desiredLeftEye[1]
	M[0, 2] += (tX - eyesCenter[0])
	M[1, 2] += (tY - eyesCenter[1])

	# apply the affine transformation
	(w, h) = (desiredFaceWidth, desiredFaceHeight)
	output = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC)

	# return the aligned face
	return output

def face_prediction(clf, face_encodings, PROB_THRESH):
	
	face_encodings = np.array(face_encodings).reshape(1,-1) 
	probas = clf.predict_proba(np.array(face_encodings))
	max_probas = np.max(probas, axis=1) 
	is_recognized = [max_probas > PROB_THRESH]

	max_probas = "{0:.2f}".format(round(max_probas[0].tolist(), 2))
	prediction = str(clf.predict(face_encodings)[0])

	if is_recognized[0][0]:
		out_pred = prediction, max_probas
	else:
		out_pred = 'Other', max_probas

	return out_pred

def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()

def _main():
	
	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	cnn_face_detector_path = '../models/mmod_human_face_detector.dat'
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detector_path)

	# Initialize dlib shape predictor
	predictor_path = '../models/shape_predictor_68_face_landmarks.dat'
	shape_predictor = dlib.shape_predictor(predictor_path)

	# Initialize cnn face detector
	face_rec_model_path = '../models/dlib_face_recognition_resnet_model_v1.dat'
	facerec = dlib.face_recognition_model_v1(face_rec_model_path)

	# Load face SVM based face predictor
	#model_save_path='../models/svc_test3.pkl'
	model_save_path='../models/model_all_217.pkl'
	with open(model_save_path, 'rb') as f:
		clf = pickle.load(f)

	save_out_path = '../images/face_rec_testing/15-05-2018/'
	margin = 10

	#image_size = (640, 480)
	#fourcc = cv2.VideoWriter_fourcc('M','J','P','G') #opencv3
	#videoWriter = cv2.VideoWriter('Fractal_Face_REC.avi', fourcc, 15.0, image_size)
	
	DETECTION_MODEL = 'cnn' # or 'cnn'
	UPSAMPLE_FACTOR = 1
	PROB_THRESH = 0.60

	#video_capture = cv2.VideoCapture('http://172.16.6.116:8080/video?dummy=param.mjpg')
	'''
	youtube_url = 'https://www.youtube.com/watch?v=wzUrLZIHtUY'
	videoPafy = pafy.new(youtube_url)
	best = videoPafy.getbest(preftype="mp4")
	video_capture = cv2.VideoCapture(best.url)
	'''
	#rtmp_url = 'rtmp://172.16.7.161/live/test'
	try:
		video_capture = cv2.VideoCapture(2)
	except IOError:
		video_capture = cv2.VideoCapture(0)
	
	video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	fps = video_capture.get(cv2.CAP_PROP_FPS)
	print "FPS: ", fps
	process_this_frame = True
	frame_num = -1

	if not os.path.exists(save_out_path+'blurry'):
		os.mkdir(save_out_path+'blurry')

	if not os.path.exists(save_out_path+'tiny'):
		os.mkdir(save_out_path+'tiny')

	if not os.path.exists(save_out_path+'others'):
		os.mkdir(save_out_path+'others')
	
	while(True):

		ret, frame = video_capture.read()
		if not ret:
			print ret
			continue

		#print ret
		height, width, _ = frame.shape
		frame_num += 1
		save = False

		if frame_num % 3 == 0:

			pil_image = Image.fromarray(frame)
			draw = ImageDraw.Draw(pil_image)
			font = ImageFont.truetype(font='/home/rajneesh/DLProjects/Utilities/font/FiraMono-Medium.otf', size=np.floor(5e-2 * height + 0.5).astype('int32'))
			thickness = (width + height) // 300

			if DETECTION_MODEL == 'cnn':
				detector = cnn_face_detector

			dets = face_detection(detector, frame, UPSAMPLE_FACTOR)
			if len(dets) > 0:
				save = True

			im_save_flag = True
			# Now process each face we found.
			for k, d in enumerate(dets):
				
				if DETECTION_MODEL == 'cnn':
					left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
				else:
					left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()

				top = max(top-margin, 0)
				left = max(left-margin, 0)
				bottom = min(height, bottom+margin)
				right = min(width, right+margin)

				time_in_millis = int(round(time.time() * 1000))
				current_face = frame[top:bottom, left:right]

				if right-left < 70 or bottom-top < 70:
					cv2.imwrite(save_out_path+'tiny/frame_num_'+str(frame_num)+'_'+str(time_in_millis)+'_1.png', current_face)
					continue
		
				gray = cv2.cvtColor(current_face, cv2.COLOR_BGR2GRAY)
				fm = variance_of_laplacian(gray)			
				# if fm < 40:
				# 	print "Blurry Image"
				# 	cv2.imwrite(save_out_path+'blurry/frame_num_'+str(frame_num)+'_'+str(time_in_millis)+'_1.png', current_face)
				# 	continue

				# Get the landmarks/parts for the face in box d.
				dd = dlib.rectangle(left, top, right, bottom)
				shape = shape_predictor(frame, dd)	
				face_descriptor = facerec.compute_face_descriptor(frame, shape_predictor(frame, dd), 5)	

				'''
				aligned_face = align(shape_predictor, frame, gray_img, dd)
				al_height, al_width, _ = aligned_face.shape
				margin = 10
				dd1 = dlib.rectangle(margin+20, margin, al_width-margin, al_height-margin)

				original_face = frame[top:bottom, left:right]

				if (right-left > 120 and bottom-top> 120):
					original_face = cv2.resize(original_face, (256, 256))
					al_resized = cv2.resize(aligned_face[margin+50:al_height-margin, margin:al_width-margin], (256, 256))
					original_face = np.hstack((original_face, aligned_face, al_resized))
					cv2.imshow('FaceRecognition1', original_face)				
				shape = shape_predictor(aligned_face, dd1)

				# Compute face encoding (128D vectors)
				#face_descriptor = facerec.compute_face_descriptor(aligned_face, shape, 10)
				'''

				(predicted_face, pred_probablity) = face_prediction(clf, face_descriptor, PROB_THRESH)
				label = '{0:}_{1:}'.format(predicted_face, pred_probablity)
				
				#label = predicted_face
				label_size = draw.textsize(label, font)
				label_w, label_h = label_size

				for i in range(thickness):
					draw.rectangle([left + i, top + i, right - i, bottom - i], outline=(255, 255, 255))
									
				new_bottom = min(bottom+label_h+2, height-2)
				label_origin_bottom = new_bottom-label_h-2

				if label_w < (right - left):
					label_w = (right - left)

				draw.rectangle([left, label_origin_bottom, left+label_w, new_bottom], fill=(0, 0, 150))					
				draw.text(np.array([left, bottom - 2]), label, fill=(255, 255, 255), font=font)	

				# Save faces into respective folders
				if predicted_face == 'Other':
					 cv2.imwrite(save_out_path+'others/'+str(time_in_millis)+'_'+str(label)+'_1.png', current_face)
				else:
					if not os.path.exists(save_out_path+predicted_face):
						os.mkdir(save_out_path+predicted_face)

					cv2.imwrite(save_out_path+predicted_face+'/'+str(time_in_millis)+'_'+str(label)+'_1.png', current_face)

			# Save images into respective folders
			if save:
				time_in_millis = int(round(time.time() * 1000))
				cv2.imwrite(save_out_path+'raw_frames/frame_'+str(frame_num)+'_'+str(time_in_millis)+'_1.png', np.array(pil_image))

		#img_display = cv2.resize(np.array(pil_image), image_size)
		cv2.imshow('FaceRecognition_1', np.array(pil_image))

		#process_this_frame = not process_this_frame
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

if __name__ == '__main__':
	_main()

