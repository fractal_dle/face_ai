from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework import status

from .serializers import EmailSerializer

class EmailView(APIView):
    parser_classes = (MultiPartParser, FormParser,)
 
    def post(self, request, *args, **kwargs):
    	
    	serializer = EmailSerializer(data=request.data)
    	if serializer.is_valid():
    		return serializer.save()
    	else:
    		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



