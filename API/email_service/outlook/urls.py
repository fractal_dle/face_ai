from django.conf.urls import url
from .views import EmailView
urlpatterns = [
  url(r'^calendar/$', EmailView.as_view(), name='outlook'),
]
