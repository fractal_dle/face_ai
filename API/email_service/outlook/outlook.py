import requests
import json
import time,pytz
from datetime import datetime,timedelta
from dateutil.tz import *

def data(meet_id,subject,location,start,end,organizer):
    calendar={}
    calendar['meet_id'] = meet_id
    calendar['subject'] = subject
    calendar['location'] = location
    calendar['start'] = start
    calendar['end'] = end
    calendar['organizer'] = organizer
    return calendar

def events_util(user_mail,zone,start_day,start_hour,start_minute,end_day,end_hour,end_minute):
	url = "http://172.16.2.166:8197/get_usercalendar1/"	
	payload = "email="+user_mail+"%40fractalanalytics.com&select=Subject%2COrganizer%2CLocation%2CStart%2CEnd&start_date="+start_day+"T"+start_hour+"%3A"+start_minute+"&end_date="+end_day+"T"+end_hour+"%3A"+end_minute
	# print(payload)
	headers = { 'content-type': "application/x-www-form-urlencoded",'cache-control' : 'no-cache','Prefer':'outlook.timezone='+zone}#\"Asia/Kolkata\"
	response = requests.post(url, data=payload, headers=headers)
	if response.status_code==200:
		try:
			res=[]
			events=response.json()['value']
			for i in events:
				cal=data(i['@odata.etag'],i['subject'],i['location']['displayName'],i['start'],i['end'],i['organizer']['emailAddress'])
				res.append(cal)
			return {'value':res,'status_code':200}
		except:
		 	return {'status_code':400}
	else:
		return {'status_code':response.status_code}    

def events(user_mail,zone,min):
	user_mail=user_mail.split('@')[0]
	tz = pytz.timezone('Asia/Kolkata')
	now = datetime.now(tz)
	#now=now.astimezone(pytz.timezone('UTC'))
	day=now.strftime('%Y-%m-%d')
	hour=now.strftime('%H')
	minute=now.strftime('%M')
	end = now+timedelta(minutes=min)
	#print (user_mail,day,hour,minute,end.strftime('%Y-%m-%d'),end.strftime('%H'),end.strftime('%M'))
	zone='\"'+zone+'\"'
	return events_util(user_mail,zone,day,hour,minute,end.strftime('%Y-%m-%d'),end.strftime('%H'),end.strftime('%M'))