from rest_framework import serializers
from django.http import JsonResponse
from .outlook import events

class EmailSerializer(serializers.Serializer):
    email=serializers.EmailField(max_length=None, min_length=None, allow_blank=False)
    minutes=serializers.IntegerField(max_value=2000, min_value=1)
    timezone=serializers.CharField(max_length=None, min_length=None, allow_blank=True, trim_whitespace=True)

    def create(self, validated_data):
        email=validated_data.get('email')
        minutes=validated_data.get('minutes')
        timezone=validated_data.get('timezone')
        res=events(email,timezone,minutes)
        if res['status_code']==200:
        	return JsonResponse({'context':'calender details','value':res['value']},status=201,safe=False)
        else:
        	return JsonResponse({'status_code':res['status_code']},status=res['status_code'])