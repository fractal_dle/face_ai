from django.conf.urls import url
from .views import DetectView,PredictView,TrainView

urlpatterns = [
  url(r'^detect/$', DetectView.as_view(), name='file-detect'),
  url(r'^predict/$', PredictView.as_view(), name='file-predict'),
  url(r'^train/$', TrainView.as_view(), name='file-train'),
]
