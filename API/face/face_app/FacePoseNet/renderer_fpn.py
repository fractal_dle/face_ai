import csv
import lmdb
import sys
import numpy as np
import cv2
import os

this_path = os.path.dirname(os.path.abspath(__file__))
render_path = this_path+'/face_renderer/'
sys.path.append(render_path)
#from .FacePoseNet.face_renderer import myutil
#from .FacePoseNet.face_renderer import config
#from .FacePoseNet import get_Rts as getRts
#from .FacePoseNet.face_renderer import camera_calibration as calib
#from .FacePoseNet.face_renderer import ThreeD_Model,renderer as renderer_core

import myutil
import config
import camera_calibration as calib
import ThreeD_Model
import renderer as renderer_core
import get_Rts as getRts

def render_fpn(FACE_POS_DIR,inputFile, output_pose_db, outputFolder):
    opts = config.parse()
    newModels = opts.getboolean('renderer', 'newRenderedViews')
    if opts.getboolean('renderer', 'newRenderedViews'):
        pose_models_folder = '/models3d_new/'
        pose_models = [
         'model3D_aug_-00_00', 'model3D_aug_-22_00', 'model3D_aug_-40_00']
    else:
        pose_models_folder = '/models3d/'
        pose_models = [
         'model3D_aug_-00', 'model3D_aug_-40']
    nSub = 10
    allModels = myutil.preload(render_path, pose_models_folder, pose_models, nSub)

    pose_env = lmdb.open(output_pose_db, readonly=True)
    pose_cnn_lmdb = pose_env.begin()

    with open(inputFile, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        lines = csvfile.readlines()
        for lin in lines:
            ### key1, image_path_key_1
            image_key = lin.split(',')[0]
            if 'flip' in image_key:
                continue

            image_path = lin.split(',')[-1].rstrip('\n')
            img = cv2.imread(image_path, 1)
            #pose_Rt_raw = pose_cnn_lmdb.get( image_key )
            pose_Rt_raw = pose_cnn_lmdb.get( image_key.encode() )
            #pose_Rt_flip_raw = pose_cnn_lmdb.get(image_key + '_flip')
            pose_Rt_flip_raw = pose_cnn_lmdb.get((str(image_key.encode()) + '_flip').encode())

            if pose_Rt_raw is not None:
                pose_Rt = np.frombuffer( pose_Rt_raw, np.float32 )
                if pose_Rt_flip_raw is not None:
                    print(pose_Rt_flip_raw)
                    pose_Rt_flip = np.frombuffer( pose_Rt_flip_raw, np.float32 )
                else:
                    pose_Rt_flip=None
                
                yaw = myutil.decideSide_from_db(img, pose_Rt, allModels)                
                print(("type(yaw)", type(yaw)))
                if yaw < 0: # Flip image and get the corresponsidng pose
                    img = cv2.flip(img,1)
                    if pose_Rt_flip is not None:
                        pose_Rt = pose_Rt_flip

                listPose = myutil.decidePose(yaw, opts, newModels)
                print(("listPose: ", listPose))
                ## Looping over the poses
                for poseId in listPose:
                    posee = pose_models[poseId]
                    ## Looping over the subjects
                    for subj in [10]:
                        pose =   posee + '_' + str(subj).zfill(2) +'.mat'
                        print(('> Looking at file: ' + image_path + ' with ' + pose))
                        # load detections performed by dlib library on 3D model and Reference Image
                        print(("> Using pose model in " + pose))
                        ## Indexing the right model instead of loading it each time from memory.
                        model3D = allModels[pose]
                        eyemask = model3D.eyemask
                        # perform camera calibration according to the first face detected
                        proj_matrix, camera_matrix, rmat, tvec = calib.estimate_camera(model3D, pose_Rt, pose_db_on=True)
                        ## We use eyemask only for frontal
                        if not myutil.isFrontal(pose):
                            eyemask = None
                        ##### Main part of the code: doing the rendering #############
                        rendered_raw, rendered_sym, face_proj, background_proj, temp_proj2_out_2, sym_weight = renderer_core.render(img, proj_matrix,\
                                                                                                 model3D.ref_U, eyemask, model3D.facemask, opts)
                        ########################################################
                        if myutil.isFrontal(pose):
                            rendered_raw = rendered_sym
                        ## Cropping if required by crop_models
                        #rendered_raw = myutil.cropFunc(pose,rendered_raw,crop_models[poseId])
                        ## Resizing if required
                        #if resizeCNN:
                        #    rendered_raw = cv2.resize(rendered_raw, ( cnnSize, cnnSize ), interpolation=cv2.INTER_CUBIC )
                        ## Saving if required
                        if opts.getboolean('general', 'saveON'):
                            image_path_name = '_'.join(image_key.split('_')[:3])
                            #subjFolder = outputFolder + '/'+ image_key.split('_')[0]
                            #subjFolder = outputFolder + '/'+ image_path_name
                            subjFolder = outputFolder
                            #print subjFolder
                            #import sys; sys.exit(1)
                            myutil.mymkdir(subjFolder)
                            savingString = subjFolder +  '/' + image_key +'_rendered_'+ pose[8:-7]+'_'+str(subj).zfill(2)+'.jpg'
                            cv2.imwrite(savingString, rendered_raw)
    return
