import sys
import os
import csv
import numpy as np
import cv2
import math
import os

## To make tensorflow print less (this can be useful for debug though)
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
#import ctypes; 

pose_dir = os.path.dirname(os.path.abspath(__file__))
#print(pose_dir,"pose_dir")
sys.path.append(pose_dir)
import pose_utils
import myparse
import renderer_fpn
import get_Rts as getRts

def face_pos(FACE_POS_DIR,input_file,output_render,cnn_detector_path):
	print(FACE_POS_DIR,input_file,output_render,"op",cnn_detector_path,"cnn_det")
	######## TMP FOLDER #####################
	_tmpdir = FACE_POS_DIR+'/tmp/'#os.environ['TMPDIR'] + '/'
	if not os.path.exists( _tmpdir):
	    os.makedirs( _tmpdir )
	#########################################
	##INPUT/OUTPUT
	outpu_proc = FACE_POS_DIR+'/output_preproc.csv'
	output_pose_db =  FACE_POS_DIR+'/output_pose.lmdb'

	#output_render = '../images/sample_10_images'
	#################################################
	_alexNetSize = 227
	_factor = 0.25 #0.1

	# ***** please download the model in https://www.dropbox.com/s/r38psbq55y2yj4f/fpn_new_model.tar.gz?dl=0 ***** #
	model_folder = FACE_POS_DIR+'/fpn_new_model/'
	model_used = 'model_0_1.0_1.0_1e-07_1_16000.ckpt' #'model_0_1.0_1.0_1e-05_0_6000.ckpt'
	lr_rate_scalar = 1.0
	if_dropout = 0
	keep_rate = 1
	################################
	
	data_dict = myparse.parse_input(input_file,cnn_detector_path)
	#data_dict={'_0': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_485.jpg', 'x': 317.0, 'y': 45.0, 'width': 244.0, 'height': 245.0}, '_1': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_28.jpg', 'x': 300.0, 'y': 143.0, 'width': 204.0, 'height': 204.0}, '_2': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_450.jpg', 'x': 119.0, 'y': 94.0, 'width': 244.0, 'height': 245.0}, '_3': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_1071.jpg', 'x': 342.0, 'y': 70.0, 'width': 244.0, 'height': 244.0}, '_4': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_1.jpg', 'x': 465.0, 'y': 70.0, 'width': 245.0, 'height': 244.0}, '_5': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_104.jpg', 'x': 256.0, 'y': 218.0, 'width': 170.0, 'height': 170.0}, '_6': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_645.jpg', 'x': 630.0, 'y': 143.0, 'width': 204.0, 'height': 204.0}, '_7': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_446.jpg', 'x': 342.0, 'y': 144.0, 'width': 244.0, 'height': 245.0}, '_8': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_1030.jpg', 'x': 259.0, 'y': 226.0, 'width': 203.0, 'height': 203.0}, '_9': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_741.jpg', 'x': 317.0, 'y': 144.0, 'width': 244.0, 'height': 245.0}, '_10': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_651.jpg', 'x': 565.0, 'y': 169.0, 'width': 244.0, 'height': 244.0}, '_11': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_616.jpg', 'x': 73.0, 'y': 205.0, 'width': 204.0, 'height': 204.0}, '_12': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_882.jpg', 'x': 292.0, 'y': 169.0, 'width': 245.0, 'height': 244.0}, '_13': {'file': '/home/fractaluser/django_api/api/latest/face/media/frames/F00000_shreya/_0_890.jpg', 'x': 229.0, 'y': 231.0, 'width': 142.0, 'height': 142.0}}

	#print(data_dict)
	## Pre-processing the images
	print('> preproc')
	pose_utils.preProcessImage( _tmpdir, data_dict, FACE_POS_DIR+'/', _factor, _alexNetSize, outpu_proc)
	## Running FacePoseNet
	print('> run')
	## Running the pose estimation
	getRts.esimatePose(model_folder, outpu_proc, output_pose_db, model_used, lr_rate_scalar, if_dropout, keep_rate, use_gpu=True)
	renderer_fpn.render_fpn(FACE_POS_DIR,outpu_proc, output_pose_db, output_render)
	return True
