import csv
import dlib
import sys
import os
import cv2

def parse_input(input_dir,cnn_detector_path):
	data_dict = dict()
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_detector_path)

	'''
	reader = csv.DictReader(open(input_file,'r'))
	#### Reading the metadata into a DICT
	for line in reader:
		key = line['ID']
		data_dict[key] = {'file' :  line['FILE']  ,\
						  'x' : float( line['FACE_X'] ),\
						  'y' : float( line['FACE_Y'] ),\
						  'width' : float( line['FACE_WIDTH'] ),\
						  'height' : float( line['FACE_HEIGHT'] ),\
					  }
	'''

	# Code to write custom dictionary
	#all_dir = os.listdir(input_dir)
	#for each_dir in all_dir:
	all_images = os.listdir(input_dir)
		#print all_images
	for index, each_image in enumerate(all_images):
		image_path = input_dir+each_image
		dets = cnn_face_detector(cv2.imread(image_path), 1)

		for i, d in enumerate(dets):
			key = input_dir.split('/')[-1]+'_'+str(index)
			data_dict[key] = {'file' :  input_dir+each_image, 'x' : float(d.rect.left()), 'y' : float(d.rect.top()), 'width' : float(d.rect.right() - d.rect.left()), 'height' : float(d.rect.bottom() - d.rect.top())}

	return data_dict
