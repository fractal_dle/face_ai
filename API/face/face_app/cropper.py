#!/usr/bin/python

import sys
import os
import dlib
import pickle
import cv2
import numpy as np
import random
import time
from PIL import Image
import pandas as pd
import errno
from .constants import FACE_POS_DIR,CROP_DIR,MEDIA_DIR,FRAMES_DIR,FRAMES_AUG_DIR,cnn_face_detector_path

def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def crop(dir_names,cnn_face_detector_path):
	
	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detector_path)

	DETECTION_MODEL = 'cnn' # or 'cnn'
	UPSAMPLE_FACTOR = 1
	margin = 10

	label_count = 0
	for dir in dir_names:
		try:
			os.makedirs(CROP_DIR+'/'+dir)
		except OSError as e:
			if e.errno != errno.EEXIST:
				return False
			else:
				pass
					
		for image_name in os.listdir(FRAMES_AUG_DIR+'/'+dir):
			image_path = FRAMES_AUG_DIR+'/'+dir+'/'+image_name
			try:
				image = cv2.imread(image_path)
				if DETECTION_MODEL == 'cnn':
					detector = cnn_face_detector	

				dets = face_detection(detector, image, UPSAMPLE_FACTOR)
				# Now process each face we found.
				face_encodings = []
				for k, d in enumerate(dets):
						
					if DETECTION_MODEL == 'cnn':
						left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
					else:
						left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
						
						#save_image_name = int(round(time.time() * 1000))
					cropped_img = image[top-margin:bottom+margin, left-margin:right+margin]
					height, width = cropped_img.shape[:2]
					top = max(0, top+2)
					left = max(0, left+2)
					right = min(width, right+2)
					bottom = min(bottom, right+2)

					cv2.imwrite(CROP_DIR+'/'+dir+'/'+image_name, cropped_img)
				
			except Exception as e:
				return False
				# print( image_path, e)
				# continue
	return True

if __name__ == '__main__':
	crop(sys.argv[1],sys.argv[2])

