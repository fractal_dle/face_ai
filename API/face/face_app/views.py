from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework import status
from .serializers import FileListSerializer,VideoListSerializer
from .detection import detect_image
from .prediction import predict_image
import json

class DetectView(APIView):
    parser_classes = (MultiPartParser, FormParser)
 
    def post(self, request, *args, **kwargs):
    	# print(request.data)
    	file_serializer = FileListSerializer(data=request.data)
    	if file_serializer.is_valid():
    		res=file_serializer.save()
    		if res.status_code==201: #or res.status_code==200:
    			jsn=json.loads(res.content.decode('utf-8'))
    			res = detect_image(str(jsn['value']))
    			return JsonResponse({'id':jsn['id'],'value':{'img_size':{'height':res[1],'width':res[2]},'faces':res[0]}},status=201,safe=False)
    		return res
    	else:
    		return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PredictView(APIView):
    parser_classes = (MultiPartParser, FormParser)
 
    def post(self, request, *args, **kwargs):
    	# print(request.data)
    	file_serializer = FileListSerializer(data=request.data)
    	if file_serializer.is_valid():
    		res=file_serializer.save()
    		if res.status_code==201: #or res.status_code==200:
    			jsn=json.loads(res.content.decode('utf-8')) 
    			res = predict_image(str(jsn['value']))
    			return JsonResponse({'id':jsn['id'],'value':{'img_size':{'height':res[1],'width':res[2]},'faces':res[0]}},status=201,safe=False)
    		return res
    	else:
    		return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#Upload videos with fractalId_FirstName_SecondName
class TrainView(APIView):
    parser_classes = (MultiPartParser, FormParser,)
 
    def post(self, request, *args, **kwargs):
    	
    	file_serializer = VideoListSerializer(data=request.data)
    	if file_serializer.is_valid():
    		return file_serializer.save()
    	else:
    		return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)