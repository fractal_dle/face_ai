import dlib
import cv2
import pickle
import numpy as np
import os
from face_app.constants import MEDIA_DIR,face_rec_model_path,cnn_face_detector_path,predictor_path,model_save_path

def data(k,predicted_id,predicted_name,pred_probablity,left,top,right,bottom):
    face={}
    rect={}
    #face['faceId'] = k
    face['empId'] = predicted_id
    face['name'] = predicted_name
    face['confidence'] = pred_probablity
    rect['left'] = left
    rect['top'] = top
    rect['right'] = right
    rect['bottom'] = bottom
    face['faceLocation'] = rect	
    return face

def face_prediction(clf, face_encodings, face_locations, PROB_THRESH = .5):
	
	probas = clf.predict_proba(np.array(face_encodings))
	max_probas = np.max(probas, axis=1) 
	
	is_recognized = [max_probas[i] > PROB_THRESH for i in range(max_probas.shape[0])]
	# predict classes and cull classifications that are not with high confidence
	return [(pred, loc, prob) if rec else ("Other", loc, prob) for pred, loc, prob, rec in zip(clf.predict(face_encodings), face_locations, max_probas, is_recognized)]


def face_detection(detector, image, upsample_factor):
    return detector(image, upsample_factor)

def predict_image(path):
	detection_model = 'dlib'
	upsample_factor= 1
	PROB_THRESH=0.4

	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	#cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detector_path)

	# Initialize cnn face rec
	facerec = dlib.face_recognition_model_v1(face_rec_model_path)

	shape_predictor = dlib.shape_predictor(predictor_path)

	with open(model_save_path, 'rb') as f:
		u = pickle._Unpickler(f)
		u.encoding = 'latin1'
		clf = u.load()

	imgPath=os.path.join(MEDIA_DIR, str(path))
	frame = cv2.imread(imgPath)
	height, width, _ = frame.shape
	if detection_model == 'cnn':
		detector = cnn_face_detector
	dets = face_detection(detector, frame, upsample_factor)

	# Now process each face we found.
	res = []
	face_encodings=[]
	face_locations=[]
	for k, d in enumerate(dets):
		if detection_model == 'cnn':
			left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
		else:
			left, top, right, bottom =  d.left(), d.top(), d.right(), d.bottom()
		# Get the landmarks/parts for the face in box dd.
		dd = dlib.rectangle(left, top, right, bottom)
		shape = shape_predictor(frame, dd)

		# Compute face encoding (128D vectors)
		face_descriptor = facerec.compute_face_descriptor(frame, shape, 10)
		face_encodings.append(face_descriptor)
		face_locations.append((left, top, right, bottom))
		#print( face_locations)

	if len(face_encodings) > 0:
		preds = face_prediction(clf, face_encodings, face_locations, PROB_THRESH)
		#print(type( preds[0][0]))
		#.decode('utf-8')
		for pred in preds:
			predictions = pred[0].split('_')
			#print (pred)
			if predictions[0]!='Other':
				predicted_id = predictions[0]
				predicted_name='_'.join(predictions[1:])
				pred_probablity = pred[2]
			else:
				predicted_id = 'F00000'
				predicted_name='Other'
				pred_probablity = pred[2]
			coordinates = pred[1]
			left, top, right, bottom = coordinates[0], coordinates[1], coordinates[2], coordinates[3]
			face=data(k,predicted_id,predicted_name,pred_probablity,left,top,right,bottom)
			res.insert(k,face)
	return (res,height,width)

#if __name__ == '__main__':
	#6f639ee11ce3e8928fa8152ac3cda6aa
#	print(predict_image('6f639ee11ce3e8928fa8152ac3cda6aa.jpg'))