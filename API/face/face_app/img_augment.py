import os
import sys
import glob
from scipy import misc
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import errno
from .constants import MEDIA_DIR,VIDEOS_DIR,FRAMES_DIR,FRAMES_AUG_DIR

def make_dir(base_dir,dir_name):
	sp = base_dir+'/'+dir_name
	try:
		os.makedirs(sp)
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise
		else:
			pass
	return sp

def augment(dir_names,gen_images):
	spaths=[make_dir(FRAMES_AUG_DIR,dir) for dir in dir_names]

	datagen = ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.20,
        height_shift_range=0.15,
        shear_range=0.10,
        zoom_range=0.10,
        #horizontal_flip=True,
        zca_whitening=True,
		#zca_epsilon=1e-6,		
        fill_mode='nearest')

	for dir in dir_names:
		no_of_images=len(glob.glob(os.path.join(FRAMES_DIR,dir, "*")))
		#print(no_of_images)
		if no_of_images!=0:
			image_factor=int(gen_images)/int(no_of_images)
			#print( '# images in '+dir+' : '+str(no_of_images))
			#print( 'factor : '+ str(image_factor))
			for image in glob.glob(os.path.join(FRAMES_DIR,dir,"*")):
				img = misc.imread(image) # this is a PIL image
				x = img_to_array(img)  # this is a Numpy array with shape (3, 150, 150)
				x = x.reshape((1,) + x.shape)  # this is a Numpy array with shape (1, 3, 150, 150)
				#no_of_images_dir=len(glob.glob(os.path.join(saveToDir, "*.jpg")))
				# the .flow() command below generates batches of randomly transformed images
				i = 0
				for batch in datagen.flow(x, batch_size=1,save_to_dir=FRAMES_AUG_DIR+'/'+dir, save_format='jpg'):
					i += 1	
					if i > image_factor:
						break  # otherwise the generator would loop indefinitely
					if len(glob.glob(os.path.join(FRAMES_AUG_DIR+'/'+dir, "*"))) >=int(gen_images):
						print( 'limit reached')
						print( len(glob.glob(os.path.join(FRAMES_AUG_DIR+'/'+dir, "*"))))
						break
				if len(glob.glob(os.path.join(FRAMES_AUG_DIR+'/'+dir, "*"))) >=int(gen_images):
					print( 'limit reached')
					print( len(glob.glob(os.path.join(FRAMES_AUG_DIR+'/'+dir, "*"))))
					break
	return True



if __name__=="__main__":
	augment(sys.argv[1],sys.argv[2])