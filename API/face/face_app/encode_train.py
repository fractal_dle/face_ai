import dlib
import sys,os,pickle,csv,cv2
import pandas as pd
from datetime import datetime
import subprocess
from .constants import *
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

def train_ml_model(df, model_file):	
	y = df['label']
	X = df.drop('label',  axis=1)
	#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
	# print("classes",len([name for name in set(y)]))
	svm = SVC(kernel='linear', C=10, gamma=0.001, probability=True)
	svm.fit(X, y)
	with open(model_file, 'wb') as fp:
		pickle.dump(svm, fp)
	# Create a list of class names
	#class_names = [name for name in set(y)]
	#print classification_report(y, y_pred, target_names=class_names)
	#print confusion_matrix(y, y_pred, labels=range(len(class_names)))


def face_encoder(dir_names,shape_predictor, facerec_model):
	know_face_encoding = []
	known_faces = []
	#column_list = list(range(128))
	column_list = [str(i) for i in range(128)]
	column_list.append('label')
	df = pd.DataFrame(columns=column_list)
	i = 0

	for dir in dir_names:
		all_images = os.listdir(CROP_DIR+'/'+dir)
		for each_image in all_images:
			try:
				image_path  = CROP_DIR+'/'+dir+'/'+each_image
				image = cv2.imread(image_path)		
				height, width, _ = image.shape
				dd = dlib.rectangle(0, 0, width, height)
				shape = shape_predictor(image, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = list(facerec_model.compute_face_descriptor(image, shape))
				face_descriptor.append(str(dir))
				df.loc[i] = face_descriptor
				i += 1
			except:
				continue
	return df

def get_latest_encoding_filename(encoding_dir):
	min=1000
	day_tag=0
	version=-100
	cur_date=int(''.join(datetime.now().date().isoformat().split('-')))
	for i in os.listdir(encoding_dir):
		temp= cur_date- int(''.join(i.split('_')[0].split('-')))
		v=i.split('_')[1]
		if temp==0:
			day_tag=1
			if version < v:
				file=i
				version=v
				min=temp
		elif temp<min:
			min=temp
			file=i
			version=v
	return file,day_tag,version

def get_new_encoding_filename(file,add_count,day_tag,version):
	fname=file.split('_')
	fname[0]=datetime.now().date().isoformat()
	fname[4]=str(int(fname[4])+add_count)
	if day_tag==1:
		fname[1]=str(int(version)+1)
	else:
		fname[1]='1'
	return '_'.join(fname)

def main(dir_names):
	try:
		# Initialize dlib shape predictor
		shape_predictor = dlib.shape_predictor(predictor_path)

		# Initialize cnn face detector
		facerec_model = dlib.face_recognition_model_v1(face_rec_model_path)
		df_new=face_encoder(dir_names, shape_predictor, facerec_model)
		file,day_tag,version=get_latest_encoding_filename(encoding_dir)
		df_old=pd.read_csv(os.path.join(encoding_dir, file))
		df_old=df_old.iloc[:,1:]
		frames=[df_old,df_new]
		df_all=pd.concat(frames, axis=0, join='outer', ignore_index=True)
		new_encoding=get_new_encoding_filename(file,len(set(df_new['label'])),day_tag,version)
		df_all.to_csv(encoding_dir+'/'+new_encoding, sep=',', encoding='utf-8')
		new_model_path=new_encoding.split('.')[0].split('_')
		new_model_path = MODEL_DIR+'/trained_models/'+new_model_path[0]+'_'+new_model_path[1]+'_model_'+new_model_path[4]+'_'+new_model_path[5]+'.pkl'
		print("This is new model path",new_model_path)
		train_ml_model(df_all, new_model_path)
		return True
	except:
		return False
	
if __name__=="__main__":
	#file,day_tag,version=get_latest_encoding_filename(encoding_dir)
	df_new = main(dir_names,file)
	#df_old=pd.read_csv(encoding_dir+file)
	# df_new.to_csv(encoding_dir+'test.csv')
	# df_new=pd.read_csv(encoding_dir+'test.csv')
	# df_new=df_new.iloc[:,1:]
	# df_old=df_old.iloc[:,1:]
	# df_all=df_old.append(df_new,ignore_index=True)
	# subprocess.Popen(['rm', '-f', encoding_dir+'test.csv'])
	#df_all.to_csv(encoding_dir+get_new_encoding_filename(file,len(set(df_new['label'])),day_tag,version), sep=',', encoding='utf-8')
