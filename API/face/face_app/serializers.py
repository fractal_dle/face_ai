from rest_framework import serializers
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.http import JsonResponse
from django.conf import settings
#from .models import File

from datetime import datetime
from functools import partial
from PIL import Image
#from urllib.parse import urlparse
import hashlib
import os
import http.client
import io
from . import preprocess
from .utils import *
from .constants import MEDIA_DIR

def status(msg):
	return JsonResponse({'error': msg}, status=400)

def hash_image(file, block_size=65536):
    # Utility function to hash images
    hasher = hashlib.md5()
    for buf in iter(partial(file.read, block_size), b''):
        hasher.update(buf)

    return hasher.hexdigest()

def hash_image1(userImage):
    mysha1 = hashlib.sha1()
    mysha1.update(userImage)
    hash_img= mysha1.hexdigest()
    return hash_img


class FileListSerializer(serializers.Serializer):
    images = serializers.ListField(child=serializers.ImageField(max_length=None, allow_empty_file=False, use_url=True,required=False), min_length=0, max_length=1)
    url=serializers.URLField(max_length=400, min_length=None, allow_blank=False,required=False)

    def create(self, validated_data):
        images = validated_data.get('images')
        url=validated_data.get('url')
        # print(len(images))
        if images:
	        for image in images:
	        	imageHash = hash_image(image)
	        	image_name = image.name
	        	image_extension = image_name.split('.')[-1]

	        	# Try to write file to volume
	        	image.seek(0)
	        	try:
	        		image.name = imageHash + '.' + image_extension
	        		path = default_storage.save(MEDIA_DIR+'/input_images/'+str(image), ContentFile(image.read()))
	        		# print(path)
	        		# print(os.path.join(MEDIA_DIR, str(path)))
        		except Exception as e:
        			return status('image save failed')

        	return JsonResponse({'id':imageHash,'value':path},status=201,safe=False)
        
        elif url:
        	#check url
        	domain, path = split_url(url)
        	if not valid_url_extension(url) or not valid_url_mimetype(url):
        		status("Invalid Image. URL must have any of .jpg/.jpeg/.png/.gif extensions")
        	
        	filename = get_url_tail(path)
        	if not image_exists(domain, path):
        		status("Error reaching the server")

        	fileObj=retrieve_image(url)
        	if not valid_image_mimetype(fileObj):
        		status("Downloaded file was not a valid image")

        	pil_image = Image.open(fileObj)
        	if not valid_image_size(pil_image)[0]:
        		status("Image is too large (> 4mb)")

        	django_file = pil_to_django(pil_image)
        	#imageHash = hash_image1(filename.encode('utf-8'))
        	imageHash = hash_image(django_file)
        	# print(imageHash)
        	image_extension = filename.split('.')[-1]
        	name=str(imageHash+'.'+image_extension)
        	path = default_storage.save(MEDIA_DIR+'/input_images/'+name, django_file)
        	return JsonResponse({'id':imageHash,'value':path},status=201,safe=False)
        else:
        	return status('No images Found')  

class VideoListSerializer ( serializers.Serializer ) :
    files = serializers.ListField(
                       child=serializers.FileField( max_length=None,
                                         allow_empty_file=False,
                                         use_url=False )
                                )

    def create(self, validated_data):
        files=validated_data.get('files')
        videos=[]
        if files:
            for file in files:
                fname  = file.name
                #Validate fname
                if valid_fname(fname):
                    #print(fname)
                    #check if label already exists

                    # Try to write file to volume
                    file.seek(0)
                    content=file.read()
                    if valid_video_mimetype(io.BytesIO(content)):
                        try:
                            path = default_storage.save(MEDIA_DIR+'/videos/'+str(fname), ContentFile(content))
                            videos.append(path.split('/')[-1])
                            # print(videos)
                        except Exception as e:
                            return status('file save failed')
                    else:
                        return status('File is not Video')
                else:
                    return status('file with '+fname+' is incorrectly named')    
            # return JsonResponse({'status':'success'},status=201,safe=False)
            res=preprocess.main(videos)
            return JsonResponse({'status':res['msg']},status=res['status'],safe=False)
        else:
            return status('No files Found')