from django.http import JsonResponse
import os,cv2,errno
from datetime import datetime
from . import img_augment
from .FacePoseNet import main_fpn
from .constants import FACE_POS_DIR,CROP_DIR,MEDIA_DIR,VIDEOS_DIR,FRAMES_DIR,FRAMES_AUG_DIR,cnn_face_detector_path,aug_img_no
from . import cropper,encode_train

def make_datadir(dir_name):
	sp = FRAMES_DIR+'/'+dir_name
	try:
		os.makedirs(sp)
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise
		else:
			pass
	return sp

def save_data(fname,spath):
	#print(fname,spath)
	n_frames = 800
	cap = cv2.VideoCapture(VIDEOS_DIR+'/'+fname)
	#print(VIDEOS_DIR+'/'+fname)
	#print(cap)
	if cap.isOpened() == False:
		return False
	try:
		num = 0
		ret=True
		while (ret) and (num<n_frames):
			ret, frame = cap.read()

			if ret and num%4==0:
				t = str(datetime.now())
				t = t.split(' ')
				t = '_'.join(t)
				cv2.imwrite(spath+'/{}frame{}.jpg'.format(t,num), frame)
			num=num+1
				
		cap.release()
		cv2.destroyAllWindows()
		return True
	except:
		return False

def main(fname_list):
	#print(FACE_POS_DIR,CROP_DIR,MEDIA_DIR,VIDEOS_DIR,FRAMES_DIR,FRAMES_AUG_DIR,cnn_face_detector_path,aug_img_no)
	dir_names=[]
	for fname in fname_list:
		if fname!='':
			dir_name=os.path.splitext(fname)[0]
			dir_names.append(dir_name)
			sp=make_datadir(dir_name)
			if not save_data(fname,sp):
				return {'msg': 'frame extraction failed','status':400}
		else:
			continue

	if img_augment.augment(dir_names,aug_img_no):
		try:
			for dir in dir_names:
				main_fpn.face_pos(FACE_POS_DIR,FRAMES_DIR+'/'+dir+'/',FRAMES_AUG_DIR+'/'+dir+'/',cnn_face_detector_path)
		except:
			return {'msg': 'augmentaion failure','status':400}
		if(cropper.crop(dir_names,cnn_face_detector_path)):
			if(encode_train.main(dir_names)):
				return {'msg': 'success','status':200}
			else:
				return {'msg': 'training failure','status':400}
		else:
			return {'msg': 'cropping failure','status':400}
	else:
		return {'msg': 'augmentaion failure','status':400} 
	return {'msg': 'success','status':200}

if __name__=="__main__":
	main(fname_list)