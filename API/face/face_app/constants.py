from django.conf import settings
import os

MODEL_DIR=settings.BASE_DIR+'/ml/models'
cnn_face_detector_path = MODEL_DIR+'/mmod_human_face_detector.dat'
face_rec_model_path=MODEL_DIR+'/dlib_face_recognition_resnet_model_v1.dat'
predictor_path=MODEL_DIR+'/shape_predictor_68_face_landmarks.dat'
model_save_path=MODEL_DIR+'/2018-06-15_1_model_248_500.pkl'
encoding_dir=settings.BASE_DIR+'/ml/encodings'


MEDIA_DIR=settings.MEDIA_ROOT
VIDEOS_DIR=MEDIA_DIR+'/videos'
FRAMES_DIR=MEDIA_DIR+'/frames'
FRAMES_AUG_DIR=MEDIA_DIR+'/frames_aug'
CROP_DIR=MEDIA_DIR+'/cropped'
APP_DIR=os.path.join(settings.BASE_DIR,'face_app')
FACE_POS_DIR= APP_DIR+'/FacePoseNet'

aug_img_no=150