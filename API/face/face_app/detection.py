import dlib
import cv2
import os
from face_app.constants import MEDIA_DIR,face_rec_model_path

def data(k,left,top,right,bottom):
    face={}
    rect={}
    face['faceId'] = k
    rect['left'] = left
    rect['top'] = top
    rect['right'] = right
    rect['bottom'] = bottom
    face['faceLocation'] = rect	
    return face

def face_detection(detector, image, upsample_factor):
    return detector(image, upsample_factor)

def detect_image(path):
	detection_model = 'dlib'
	upsample_factor= 1

	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	facerec = dlib.face_recognition_model_v1(face_rec_model_path)

	imgPath=os.path.join(MEDIA_DIR, str(path))
	frame = cv2.imread(imgPath)
	height, width, _ = frame.shape
	if detection_model == 'cnn':
		detector = cnn_face_detector
	dets = face_detection(detector, frame, upsample_factor)

	# Now process each face we found.
	res = []
	for k, d in enumerate(dets):
		if detection_model == 'cnn':
			left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
		else:
			left, top, right, bottom =  d.left(), d.top(), d.right(), d.bottom()
		face=data(k,left,top,right,bottom)
		res.insert(k,face)
	return (res,height,width)

#if __name__ == '__main__':
#	print(detect_image('IMG_20180609_004145.jpg'))