import os
import io
import re
import mimetypes,magic
import http.client
from urllib.parse import urlparse,unquote
from django.core.files.base import ContentFile
from PIL import Image
import requests

VALID_IMAGE_MIMETYPES = [
    "image"
]

VALID_IMAGE_EXTENSIONS = [
    ".jpg",
    ".jpeg",
    ".png",
    ".gif",
]

MAX_SIZE = 4*1024*1024

def split_url(url):
    parse_object = urlparse(url)
    return parse_object.netloc, parse_object.path

def get_url_tail(url):
    return url.split('/')[-1]  

def get_extension(filename):
    return os.path.splitext(filename)[1]

def valid_url_extension(url, extension_list=VALID_IMAGE_EXTENSIONS):
    return any([url.endswith(e) for e in extension_list])

def valid_url_mimetype(url, mimetype_list=VALID_IMAGE_MIMETYPES):
	mimetype, encoding = mimetypes.guess_type(url)
	if mimetype:
		return any([mimetype.startswith(m) for m in mimetype_list])
	else:
		return False

def image_exists(domain, path, check_size=False, size_limit=1024):
    try:
        conn = http.client.HTTPConnection(domain)
        conn.request('HEAD', path)
        response = conn.getresponse()
        headers = response.getheaders()
        conn.close()
    except:
        return False

    try:
        length = int([x[1] for x in headers if x[0] == 'content-length'][0])
    except:
        length = 0
    if length > MAX_SIZE:
        return False

    return response.status == 200

def valid_image_mimetype(fileObj):
    mimetype = get_mimetype(fileObj)
    if mimetype:
        return mimetype.startswith('image')
    else:
        return False

def valid_image_size(image, max_size=MAX_SIZE):
    width, height = image.size
    if (width * height) > max_size:
        return (False, "Image is too large")
    return (True, image)

def get_mimetype(fobject):
    mime = magic.Magic(mime=True)
    mimetype = mime.from_buffer(fobject.read(1024))
    fobject.seek(0)
    return mimetype

def pil_to_django(image, format="JPEG"):
    fobject = io.BytesIO()
    image.save(fobject, format=format)
    return ContentFile(fobject.getvalue())

def valid_video_mimetype(fileObj):
    mimetype = get_mimetype(fileObj)
    #print(mimetype)
    if mimetype:
        return mimetype.startswith('video')
    else:
        return False

def valid_filetype(filepath):
    filetype=magic.from_file(filepath, mime=True)
    #filetype = magic.from_buffer(content)
    #print(filetype)
    if not "video" in filetype:
        return False
    else: 
        return True

def valid_fname(fname):
    fname = fname.split('.')[:-1]
    fname='.'.join(fname)
    #print(fname)
    fsplit=fname.split('_')
    #print( fsplit[0])
    res= re.match('^f{1}\d{5}$',fsplit[0].lower())
    #print(res)
    return res

def retrieve_image(url):
    response = requests.get(url)
    return io.BytesIO(response.content)